import setuptools
from epics_tango_bridge import __version__, __description__


setuptools.setup(
    name="epics_tango_bridge",
    version=__version__,
    description=__description__,
    url="https://gitlab.com/s2innovation/epics-tango-bridge",
    author="Michal Liszcz",
    author_email="michal.liszcz@s2innovation.com",
    license="",
    packages=setuptools.find_packages(),
    install_requires=[
        'pytango',
        'pcaspy'
    ],
    tests_require=[
        'mock>=3.0.0',
        'pyepics',
        'six'
    ],
    entry_points={
        'console_scripts': [
            'epics-tango-bridge=epics_tango_bridge.bridge_server:run'
        ]
    },
    test_suite="tests.unit",
    zip_safe=False)
