import unittest
import signal

import mock

from epics_tango_bridge.signal_handler import SignalHandler


class TestSignalHandler(unittest.TestCase):

    @mock.patch("signal.signal")
    def setUp(self, signal_mock):  # pylint: disable=W0221
        self.out = SignalHandler()
        self.assertFalse(self.out.stop)
        self.assertEqual(2, signal_mock.call_count)
        signal_mock.assert_has_calls([
            mock.call(signal.SIGINT, self.out.handle_stop),
            mock.call(signal.SIGTERM, self.out.handle_stop)])

    def test_should_set_stop_flag_on_signal(self):
        SIGNUM = 4
        FRAME = object()
        self.out.handle_stop(SIGNUM, FRAME)
        self.assertTrue(self.out.stop)
