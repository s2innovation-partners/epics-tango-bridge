import unittest
import logging

import mock

from epics_tango_bridge.command_manager import \
    CommandOutManager, \
    CommandInManager


logging.disable(logging.CRITICAL)


DEVICE_NAME = "some/test/device"
CMD_NAME = "some_command"
SLOT_IN = "in"
SLOT_OUT = "out"

PVINFO_ARG_IN_OUT = {}
PVINFO_ARG_VOID_IN = {'-tg-void-in': True}


class TestCommandOutManager(unittest.TestCase):

    def setUp(self):
        self.driver = mock.NonCallableMagicMock()
        pvname = object()
        self.out = CommandOutManager(self.driver, pvname)

    def test_should_read_from_driver(self):
        self.assertEqual(self.driver.read_pv.return_value, self.out.read())
        self.driver.read_pv.assert_called_once_with()

    def test_should_ignore_write(self):
        mock.seal(self.driver)
        self.assertFalse(self.out.write(object()))


class TestCommandInManager(unittest.TestCase):  # noqa E501 pylint: disable=too-many-instance-attributes

    @mock.patch("epics_tango_bridge.command_manager.make_task")
    def setUp(self, make_task):  # pylint: disable=arguments-differ
        self.driver = mock.NonCallableMagicMock()
        self.driver_out = mock.NonCallableMagicMock()
        self.device_proxy = mock.NonCallableMagicMock()
        self.command = object()
        pvname = object()
        self.pvinfo = PVINFO_ARG_IN_OUT
        self.execution_task = mock.MagicMock()
        make_task.return_value = self.execution_task
        self.out = CommandInManager(
            self.driver,
            self.driver_out,
            self.device_proxy,
            self.command,
            pvname,
            self.pvinfo)
        make_task.assert_called_once_with(
            self.out.run_thread,
            in_thread=self.pvinfo.get("asyn", False))

    def assert_command_run_with_arg(self, value):
        self.device_proxy.command_inout.assert_called_once_with(
            self.command,
            value)

    def assert_write_input(self, argin):
        self.driver.write_pv_and_notify.assert_called_once_with(argin)

    def assert_write_output(self, argout):
        self.driver_out.write_pv_and_notify.assert_called_once_with(argout)

    def assert_command_run_and_pvs_updated(self, argin, argout):
        self.assert_command_run_with_arg(argin)
        self.assert_write_input(argin)
        self.assert_write_output(argout)
        self.assert_callback_notified()

    def assert_callback_notified(self):
        self.driver.notify_write_pv_completed()

    def test_should_read_from_driver(self):
        self.assertEqual(self.driver.read_pv.return_value, self.out.read())
        self.driver.read_pv.assert_called_once_with()

    def test_should_start_execution_task_and_return_true(self):
        VALUE = object()
        self.assertTrue(self.out.write(VALUE))
        self.execution_task.assert_called_once_with(VALUE)

    def test_should_call_command_with_one_arg_and_result_from_thread_method(
            self):
        VALUE_IN = object()
        self.out.run_thread(VALUE_IN)
        self.assert_command_run_and_pvs_updated(
            argin=VALUE_IN,
            argout=self.device_proxy.command_inout.return_value)

    def test_should_ignore_tango_command_failure_and_not_update_pvs(self):
        VALUE_IN = object()
        self.device_proxy.command_inout.side_effect = Exception()
        self.out.run_thread(VALUE_IN)
        self.assert_command_run_with_arg(VALUE_IN)
        self.driver.write_pv_and_notify.assert_not_called()
        self.driver_out.write_pv_and_notify.assert_not_called()
        self.assert_callback_notified()

    def test_should_call_command_with_void_arg_and_return_result(self):
        pvname = object()
        self.out = CommandInManager(
            self.driver,
            self.driver_out,
            self.device_proxy,
            self.command,
            pvname,
            PVINFO_ARG_VOID_IN)
        VALUE_IN = object()
        self.out.run_thread(VALUE_IN)
        self.device_proxy.command_inout.assert_called_once_with(self.command)
        self.assert_write_input(VALUE_IN)
        self.assert_write_output(self.device_proxy.command_inout.return_value)
        self.assert_callback_notified()
