import unittest

import mock

from epics_tango_bridge.task import make_task


ARGS = (1, 5.2, 'test')
KWARGS = {'a': 4, 'b': 6}


class TestTask(unittest.TestCase):

    def test_should_run_non_threaded_task_directly(self):  # noqa E501 pylint: disable=no-self-use
        fn = mock.MagicMock()
        out = make_task(fn, in_thread=False)
        out(*ARGS, **KWARGS)
        fn.assert_called_once_with(*ARGS, **KWARGS)

    @mock.patch("threading.Thread")
    def test_should_run_threaded_task_in_daemon_thread(self, thread_factory):
        fn = mock.MagicMock()
        thread = thread_factory.return_value
        daemon = mock.PropertyMock()
        type(thread).daemon = daemon
        m = mock.Mock()
        m.attach_mock(thread.start, 'start')
        m.attach_mock(daemon, 'daemon')
        out = make_task(fn, in_thread=True)
        out(*ARGS, **KWARGS)
        thread_factory.assert_called_once_with(
            target=fn,
            args=ARGS,
            kwargs=KWARGS)
        self.assertEqual(
            [mock.call.daemon(True), mock.call.start()],
            m.mock_calls)
