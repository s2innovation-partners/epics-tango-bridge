import unittest
import logging

import mock
import PyTango as tango

from epics_tango_bridge.attribute_manager import \
    AttributeManager, \
    PolledAttributeManager


logging.disable(logging.CRITICAL)


class DotDict(dict):
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


def make_change_event(
        err=False,
        value=None):
    event = mock.NonCallableMagicMock()
    event.__str__.return_value = 'event'
    event.err = err
    event.attr_value.value = value
    mock.seal(event)
    return event


def make_attr_conf_event(  # pylint: disable=too-many-arguments
        err=False,
        unit='',
        min_warning='Not specified',
        max_warning='Not specified',
        min_alarm='Not specified',
        max_alarm='Not specified',
        data_format=tango.AttrDataFormat.SCALAR,
        max_dim_x=0):
    event = mock.NonCallableMagicMock()
    event.__str__.return_value = 'event'
    event.err = err
    event.attr_conf.unit = unit
    event.attr_conf.alarms.min_warning = min_warning
    event.attr_conf.alarms.max_warning = max_warning
    event.attr_conf.alarms.min_alarm = min_alarm
    event.attr_conf.alarms.max_alarm = max_alarm
    event.attr_conf.data_format = data_format
    event.attr_conf.max_dim_x = max_dim_x
    mock.seal(event)
    return event


class TestAttributeManager(unittest.TestCase):  # noqa E501 pylint: disable=too-many-public-methods,too-many-instance-attributes

    def setUp(self):
        self.driver = mock.NonCallableMagicMock()
        self.device_proxy = mock.NonCallableMagicMock()
        self.device_proxy.read_attribute.return_value.dim_x = 0
        self.attribute = object()
        self.pvname = object()
        self.write_task = mock.MagicMock()
        self.create_attribute_manager(pvinfo={})  # pylint: disable=E1120
        self.assert_attr_conf_event_subscribed()

    @mock.patch("threading.Lock")
    @mock.patch("epics_tango_bridge.attribute_manager.make_task")
    def create_attribute_manager(self, make_task, lock_factory, pvinfo):
        self.state_lock = mock.NonCallableMagicMock()
        self.driver_lock = mock.NonCallableMagicMock()
        lock_factory.side_effect = [self.state_lock, self.driver_lock]
        make_task.return_value = self.write_task
        self.out = AttributeManager(
            self.driver,
            self.device_proxy,
            self.attribute,
            self.pvname,
            pvinfo)
        lock_factory.assert_called_once_with()
        make_task.assert_called_once_with(
            self.out.write_thread,
            in_thread=pvinfo.get("asyn", False))

    def assert_tango_read(self):
        self.device_proxy.read_attribute.assert_called_once_with(
            self.attribute)

    def assert_tango_write(self, value):
        self.device_proxy.write_attribute.assert_called_once_with(
            self.attribute,
            value)

    def assert_attr_conf_event_subscribed(self):
        self.device_proxy.subscribe_event.assert_called_once_with(
            self.attribute,
            tango.EventType.ATTR_CONF_EVENT,
            self.out.handle_attr_conf_event)

    def assert_param_info_set(  # pylint: disable=too-many-arguments
            self,
            high=0.0,
            hihi=0.0,
            lolo=0.0,
            low=0.0,
            unit=''):
        pvinfo = {
            'high': high,
            'hihi': hihi,
            'lolo': lolo,
            'low': low,
            'unit': unit
            }
        pvinfo = {k: v for k, v in pvinfo.items() if v is not None}
        self.driver.write_pv_info_and_notify(pvinfo)

    def assert_state_lock_acquired(self, times=1):
        self.assertEqual(times, self.state_lock.__enter__.call_count)
        self.assertEqual(times, self.state_lock.__exit__.call_count)

    def test_should_do_nothing_on_attr_conf_subscription_failure(self):
        self.device_proxy = mock.NonCallableMagicMock()
        self.device_proxy.subscribe_event.side_effect = Exception()
        self.create_attribute_manager(pvinfo={})  # pylint: disable=E1120
        self.assert_attr_conf_event_subscribed()

    def test_should_read_attribute_from_tango_on_read(self):
        self.assertEqual(
            self.device_proxy.read_attribute.return_value.value,
            self.out.read())
        self.assert_tango_read()
        self.assert_state_lock_acquired()

    def test_should_start_write_task_and_return_true(self):
        VALUE = object()
        self.assertTrue(self.out.write(VALUE))
        self.write_task.assert_called_once_with(VALUE)

    def test_should_write_attribute_to_tango_from_thread_method(self):
        VALUE = object()
        self.out.write_thread(VALUE)
        self.assert_tango_write(VALUE)
        self.assert_tango_read()
        self.driver.write_pv_and_notify.assert_called_once_with(
            self.device_proxy.read_attribute.return_value.value)
        self.driver.notify_write_pv_finished.assert_called_once_with()
        self.assert_state_lock_acquired(times=2)

    def test_should_ignore_failure_during_write_from_write_thread(self):
        VALUE = object()
        self.device_proxy.write_attribute.side_effect = Exception()
        self.out.write_thread(VALUE)
        self.assert_tango_write(VALUE)
        self.driver.notify_write_pv_finished.assert_called_once_with()

    def test_should_ignore_failure_during_read_from_write_thread(self):
        VALUE = object()
        self.device_proxy.read_attribute.side_effect = Exception()
        self.out.write_thread(VALUE)
        self.assert_tango_write(VALUE)
        self.assert_tango_read()
        self.driver.notify_write_pv_finished.assert_called_once_with()

    def test_should_update_unit_from_tango_if_not_specified(self):
        new_unit = object()
        self.create_attribute_manager(pvinfo={})  # pylint: disable=E1120
        self.out.handle_attr_conf_event(make_attr_conf_event(unit=new_unit))
        self.assert_param_info_set(unit=new_unit)

    def test_should_not_update_unit_from_tango_if_was_specified(self):
        old_unit = object()
        new_unit = object()
        self.create_attribute_manager(pvinfo={'unit': old_unit})  # noqa E501 pylint: disable=E1120
        self.out.handle_attr_conf_event(make_attr_conf_event(unit=new_unit))
        self.assert_param_info_set(unit=None)

    def test_should_update_max_alarm_from_tango_if_not_specified(self):
        alarm = 3435
        self.create_attribute_manager(pvinfo={})  # pylint: disable=E1120
        self.out.handle_attr_conf_event(make_attr_conf_event(max_alarm=alarm))
        self.assert_param_info_set(hihi=alarm)

    def test_should_not_update_max_alarm_from_tango_if_was_specified(self):
        alarm = 1213
        old_alarm = 7788
        self.create_attribute_manager(pvinfo={'hihi': old_alarm})  # noqa E501 pylint: disable=E1120
        self.out.handle_attr_conf_event(make_attr_conf_event(max_alarm=alarm))
        self.assert_param_info_set(hihi=None)

    def test_should_not_update_param_with_extra_info_from_initial_configuration(self):  # noqa E501
        field = 'extra_field'
        value = 'extra_value'
        self.create_attribute_manager(pvinfo={field: value})  # noqa E501 pylint: disable=E1120
        self.out.handle_attr_conf_event(make_attr_conf_event())
        self.assert_param_info_set()

    def test_should_ignore_attr_conf_event_with_error_flag(self):
        self.create_attribute_manager(pvinfo={})  # pylint: disable=E1120
        self.out.handle_attr_conf_event(make_attr_conf_event(err=True))
        self.driver.write_pv_info_and_notify.assert_not_called()

    def test_should_reset_unit_if_tango_provides_empty_value(self):
        self.create_attribute_manager(pvinfo={})  # pylint: disable=E1120
        self.out.handle_attr_conf_event(
            make_attr_conf_event(unit=tango.constants.UnitNotSpec))
        self.assert_param_info_set(unit="")

    def test_should_reshape_image_data_before_writing_to_tango(self):
        self.create_attribute_manager(pvinfo={})  # pylint: disable=E1120
        self.out.handle_attr_conf_event(make_attr_conf_event(
            data_format=tango.AttrDataFormat.IMAGE,
            max_dim_x=2))
        self.out.write_thread([1, 2, 3, 4, 5])
        self.assert_tango_write([[1, 2], [3, 4], [5, 0]])
        self.assert_state_lock_acquired(times=3)

    def test_should_update_data_shape_after_reading_from_tango(self):
        self.create_attribute_manager(pvinfo={})  # pylint: disable=E1120
        self.out.handle_attr_conf_event(make_attr_conf_event(
            data_format=tango.AttrDataFormat.IMAGE,
            max_dim_x=2))
        attribute = mock.NonCallableMagicMock()
        attribute.data_format = tango.AttrDataFormat.IMAGE
        attribute.dim_x = 3
        self.device_proxy.read_attribute.return_value = attribute
        self.out.read()
        self.out.write_thread([1, 2, 3, 4, 5])
        self.assert_tango_write([[1, 2, 3], [4, 5, 0]])
        self.assert_state_lock_acquired(times=4)


class TestPolledAttributeManager(unittest.TestCase):

    def setUp(self):
        self.driver = mock.NonCallableMagicMock()
        self.device_proxy = mock.NonCallableMagicMock()
        self.attribute = object()
        self.pvname = object()
        self.pvinfo = {}
        self.out = PolledAttributeManager(
            self.driver,
            self.device_proxy,
            self.attribute,
            self.pvname,
            self.pvinfo)
        self.assert_change_and_attr_conf_events_subscribed()

    def assert_change_and_attr_conf_events_subscribed(self):
        subscribe_attr_conf_call = mock.call(
            self.attribute,
            tango.EventType.ATTR_CONF_EVENT,
            self.out.handle_attr_conf_event)
        subscribe_change_call = mock.call(
            self.attribute,
            tango.EventType.CHANGE_EVENT,
            self.out.handle_change_event)
        self.assertEqual(
            [subscribe_attr_conf_call, subscribe_change_call],
            self.device_proxy.subscribe_event.mock_calls)

    def test_should_do_nothing_on_change_subscription_failure(self):
        self.device_proxy = mock.NonCallableMagicMock()
        self.device_proxy.subscribe_event.side_effect = [None, Exception()]
        self.out = PolledAttributeManager(
            self.driver,
            self.device_proxy,
            self.attribute,
            self.pvname,
            pvinfo={})
        self.assert_change_and_attr_conf_events_subscribed()

    def test_should_read_from_driver(self):
        self.assertEqual(self.driver.read_pv.return_value, self.out.read())
        self.driver.read_pv.assert_called_once_with()

    def test_should_store_value_from_change_callback(self):
        event = make_change_event(value=4523.12)
        self.out.handle_change_event(event)
        self.driver.write_pv_and_notify.assert_called_once_with(
            event.attr_value.value)

    def test_should_ignore_change_with_error_flag(self):
        event = make_change_event(err=True)
        self.out.handle_change_event(event)
        self.driver.write_pv_and_notify.assert_not_called()
