import unittest

import mock

from epics_tango_bridge.logger import LoggerWithPvName


MESSAGE = "my log message"
PVNAME = "my:test:pv"


class TestLoggerWithPvName(unittest.TestCase):

    @mock.patch("logging.LoggerAdapter.__init__")
    @mock.patch("logging.getLogger")
    def setUp(self, get_logger, base_init):  # pylint: disable=arguments-differ
        logger_name = object()
        self.out = LoggerWithPvName(logger_name, PVNAME)
        get_logger.assert_called_once_with(logger_name)
        base_init.assert_called_once_with(get_logger.return_value, extra={})

    def test_should_prepend_pv_name_to_message(self):
        kwargs = object()
        self.assertEqual(
            ("[{}] {}".format(PVNAME, MESSAGE), kwargs),
            self.out.process(MESSAGE, kwargs))
