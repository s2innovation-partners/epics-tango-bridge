import unittest
import logging

import mock

from epics_tango_bridge.tango_driver import TangoDriver


logging.disable(logging.CRITICAL)


DEVICE_NAME = "some/test/device"
ATTR_NAME = "some_attribute"

ATTR_REASON = "{}:attr:{}".format(DEVICE_NAME, ATTR_NAME)
FAILED_REASON = "{}:attr:failed".format(DEVICE_NAME)

PVNAME_1 = ATTR_REASON
PVNAME_2 = FAILED_REASON
PVINFO_1 = object()
PVINFO_2 = object()
PVDB = {PVNAME_1: PVINFO_1, PVNAME_2: PVINFO_2}


def make_throwing_manager_factory_mock(manager):
    def make_manager(pvname, _pvinfo):
        if pvname == PVNAME_1:
            return manager
        else:
            raise Exception()
    return make_manager


class TestTangoDriver(unittest.TestCase):

    @mock.patch("pcaspy.Driver.__init__")
    @mock.patch("epics_tango_bridge.tango_driver.ManagerFactory")
    def setUp(self, manager_factory, base_init):  # pylint: disable=W0221
        self.attribute_manager = mock.NonCallableMagicMock()
        manager_factory.return_value.side_effect = \
            make_throwing_manager_factory_mock(self.attribute_manager)
        self.out = TangoDriver(PVDB)
        base_init.assert_called_once_with()
        manager_factory.assert_called_once_with(self.out)
        manager_factory.return_value.assert_has_calls(
            [mock.call(PVNAME_1, PVINFO_1), mock.call(PVNAME_2, PVINFO_2)],
            any_order=True)
        self.assertEqual(2, manager_factory.return_value.call_count)

    def test_should_forward_read_to_attribute_manager(self):
        self.assertEqual(
            self.attribute_manager.read.return_value,
            self.out.read(ATTR_REASON))
        self.attribute_manager.read.assert_called_once_with()

    @mock.patch("pcaspy.Driver.getParam")
    def test_should_return_stored_value_if_forwarded_read_fails(
            self,
            get_param):
        self.assertEqual(get_param.return_value, self.out.read(FAILED_REASON))
        get_param.assert_called_once_with(FAILED_REASON)

    def test_should_forward_write_to_attribute_manager(self):
        VALUE = object()
        self.assertEqual(
            self.attribute_manager.write.return_value,
            self.out.write(ATTR_REASON, VALUE))
        self.attribute_manager.write.assert_called_once_with(VALUE)

    def test_should_return_false_if_forwarded_write_fails(self):
        self.assertEqual(False, self.out.write(FAILED_REASON, object()))
