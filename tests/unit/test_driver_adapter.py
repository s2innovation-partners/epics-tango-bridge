import unittest

import mock

from epics_tango_bridge.driver_adapter import \
    make_driver_adapter, \
    DummyDriverAdapter, \
    DriverAdapter


class TestMakeDriverAdapter(unittest.TestCase):

    def setUp(self):
        self.driver = mock.Mock(name="driver")
        self.pvname = mock.Mock(name="pvname")

    @mock.patch("epics_tango_bridge.driver_adapter.DummyDriverAdapter")
    def test_should_create_dummy_adapter(self, factory):
        self.assertEqual(
            factory.return_value,
            make_driver_adapter(self.driver, self.pvname, is_dummy=True))
        factory.assert_called_once_with()

    @mock.patch("epics_tango_bridge.driver_adapter.DriverAdapter")
    def test_should_create_concrete_adapter(self, factory):
        self.assertEqual(
            factory.return_value,
            make_driver_adapter(self.driver, self.pvname, is_dummy=False))
        factory.assert_called_once_with(self.driver, self.pvname)


class TestDummyDriverAdapter(unittest.TestCase):

    def setUp(self):
        self.out = DummyDriverAdapter()

    def test_should_do_nothing_on_write_pv_and_notify(self):
        value = mock.Mock(name="value")
        self.out.write_pv_and_notify(value)


class TestDriverAdapter(unittest.TestCase):

    @mock.patch("threading.Lock")
    def setUp(self, lock_factory):  # pylint: disable=arguments-differ
        self.pvname = mock.Mock(name="pvname")
        self.value = mock.Mock(name="value")
        self.info = mock.Mock(name="info")
        self.mock = mock.Mock()
        self.mock.attach_mock(mock.NonCallableMagicMock(), 'driver')
        self.mock.attach_mock(lock_factory.return_value, 'lock')
        self.out = DriverAdapter(self.mock.driver, self.pvname)
        lock_factory.assert_called_once_with()

    def test_should_read_pv_from_driver(self):
        self.assertEqual(
            self.mock.driver.getParam.return_value,
            self.out.read_pv())
        self.mock.assert_has_calls([
            mock.call.lock.__enter__(),
            mock.call.driver.getParam(self.pvname),
            mock.call.lock.__exit__(None, None, None)])

    def test_should_write_pv_and_notify_driver(self):
        self.out.write_pv_and_notify(self.value)
        self.mock.assert_has_calls([
            mock.call.lock.__enter__(),
            mock.call.driver.setParam(self.pvname, self.value),
            mock.call.driver.updatePV(self.pvname),
            mock.call.lock.__exit__(None, None, None)])

    def test_should_skip_notification_on_write_pv_failure(self):
        self.mock.driver.setParam.side_effect = Exception()
        self.out.write_pv_and_notify(self.value)
        self.mock.assert_has_calls([
            mock.call.lock.__enter__(),
            mock.call.driver.setParam(self.pvname, self.value),
            mock.call.lock.__exit__(Exception, mock.ANY, mock.ANY)])

    def test_should_do_nothing_on_write_pv_notification_failure(self):
        self.mock.driver.updatePV.side_effect = Exception()
        self.out.write_pv_and_notify(self.value)
        self.mock.assert_has_calls([
            mock.call.lock.__enter__(),
            mock.call.driver.setParam(self.pvname, self.value),
            mock.call.driver.updatePV(self.pvname),
            mock.call.lock.__exit__(Exception, mock.ANY, mock.ANY)])

    def test_should_write_pv_info_and_notify_driver(self):
        self.out.write_pv_info_and_notify(self.info)
        self.mock.assert_has_calls([
            mock.call.lock.__enter__(),
            mock.call.driver.setParamInfo(self.pvname, self.info),
            mock.call.driver.updatePV(self.pvname),
            mock.call.lock.__exit__(None, None, None)])

    def test_should_skip_notification_on_write_pv_info_failure(self):
        self.mock.driver.setParamInfo.side_effect = Exception()
        self.out.write_pv_info_and_notify(self.info)
        self.mock.assert_has_calls([
            mock.call.lock.__enter__(),
            mock.call.driver.setParamInfo(self.pvname, self.info),
            mock.call.lock.__exit__(Exception, mock.ANY, mock.ANY)])

    def test_should_do_nothing_on_write_pv_info_notification_failure(self):
        self.mock.driver.updatePV.side_effect = Exception()
        self.out.write_pv_info_and_notify(self.info)
        self.mock.assert_has_calls([
            mock.call.lock.__enter__(),
            mock.call.driver.setParamInfo(self.pvname, self.info),
            mock.call.driver.updatePV(self.pvname),
            mock.call.lock.__exit__(Exception, mock.ANY, mock.ANY)])

    def test_should_notify_write_pv_finished(self):
        self.out.notify_write_pv_finished()
        self.mock.assert_has_calls([
            mock.call.lock.__enter__(),
            mock.call.driver.callbackPV(self.pvname),
            mock.call.lock.__exit__(None, None, None)])

    def test_should_do_nothing_on_notify_write_pv_finished_failure(self):
        self.mock.driver.callbackPV.side_effect = Exception()
        self.out.notify_write_pv_finished()
        self.mock.assert_has_calls([
            mock.call.lock.__enter__(),
            mock.call.driver.callbackPV(self.pvname),
            mock.call.lock.__exit__(Exception, mock.ANY, mock.ANY)])
