import unittest

import mock

from epics_tango_bridge.manager_factory import \
    ManagerFactory, \
    make_attribute_manager, \
    make_command_in_manager, \
    make_command_out_manager


SCOPE1 = "scope1"
SCOPE2 = "scope2"

SLOT = "slot"

PVINFO_ARG_VOID_OUT = {'-tg-void-out': True}


class TestManagerFactory(unittest.TestCase):

    @mock.patch("epics_tango_bridge.manager_factory.DeviceProxyProvider")
    def setUp(self, device_proxy_provider):  # pylint: disable=W0221
        self.device_proxy_provider = device_proxy_provider.return_value
        self.driver = mock.NonCallableMagicMock()
        self.make_manager_1 = mock.MagicMock()
        self.make_manager_2 = mock.MagicMock()
        self.out = ManagerFactory(self.driver)
        device_proxy_provider.assert_called_once_with()

    def create_manager(self, pvname, pvinfo):
        managers = {
            (SCOPE1, ()): self.make_manager_1,
            (SCOPE2, (SLOT,)): self.make_manager_2}
        managers_context = mock.patch.dict(
            "epics_tango_bridge.manager_factory.SCOPE_MANAGERS",
            managers)
        with managers_context:
            return self.out(pvname, pvinfo)

    def assert_options_set(self, opts, device, target, pvname, pvinfo):  # noqa E501 pylint: disable=too-many-arguments
        self.assertEqual(device, opts.device)
        self.assertEqual(self.driver, opts.driver)
        self.assertEqual(self.device_proxy_provider.return_value, opts.proxy)
        self.assertEqual(target, opts.target)
        self.assertEqual(pvname, opts.pvname)
        self.assertEqual(pvinfo, opts.pvinfo)

    def test_should_create_manager_depending_on_scope(self):
        pvname = "device1:{}:target1".format(SCOPE1)
        pvinfo = object()
        self.assertEqual(
            self.make_manager_1.return_value,
            self.create_manager(pvname, pvinfo))
        self.make_manager_1.assert_called_once_with(mock.ANY)
        (opts,), _ = self.make_manager_1.call_args
        self.assert_options_set(opts, "device1", "target1", pvname, pvinfo)
        self.make_manager_2.assert_not_called()

    def test_should_create_manager_depending_on_scope_and_slot(self):
        pvname = "device2:{}:target2:{}".format(SCOPE2, SLOT)
        pvinfo = object()
        self.assertEqual(
            self.make_manager_2.return_value,
            self.create_manager(pvname, pvinfo))
        self.make_manager_1.assert_not_called()
        self.make_manager_2.assert_called_once_with(mock.ANY)
        (opts,), _ = self.make_manager_2.call_args
        self.assert_options_set(opts, "device2", "target2", pvname, pvinfo)


class TestManagerConstructors(unittest.TestCase):

    def create_command_in_manager(self, constructor, driver, pvinfo, is_dummy):
        opts = mock.NonCallableMagicMock()
        opts.device = "my/test/device"
        opts.target = "my_command"
        opts.pvinfo = pvinfo
        pvname_out = "{}:cmd:{}:out".format(opts.device, opts.target)
        driver_in = mock.Mock(name="driver_in")
        driver_out = mock.Mock(name="driver_out")
        driver.side_effect = [driver_in, driver_out]
        self.assertEqual(
            constructor.return_value,
            make_command_in_manager(opts))
        constructor.assert_called_once_with(
            driver_in,
            driver_out,
            opts.proxy,
            opts.target,
            opts.pvname,
            opts.pvinfo)
        driver.assert_has_calls([
            mock.call(opts.driver, opts.pvname),
            mock.call(opts.driver, pvname_out, is_dummy=is_dummy)])

    @mock.patch("epics_tango_bridge.manager_factory.make_driver_adapter")
    @mock.patch("epics_tango_bridge.manager_factory.AttributeManager")
    def test_should_create_attribute_manager(self, constructor, driver):
        opts = mock.NonCallableMagicMock()
        opts.pvinfo = {'-tg-polled': False}
        self.assertEqual(
            constructor.return_value,
            make_attribute_manager(opts))
        constructor.assert_called_once_with(
            driver.return_value,
            opts.proxy,
            opts.target,
            opts.pvname,
            opts.pvinfo)
        driver.assert_called_once_with(opts.driver, opts.pvname)

    @mock.patch("epics_tango_bridge.manager_factory.make_driver_adapter")
    @mock.patch("epics_tango_bridge.manager_factory.PolledAttributeManager")
    def test_should_create_polled_attribute_manager(self, constructor, driver):
        opts = mock.NonCallableMagicMock()
        opts.pvinfo = {'-tg-polled': True}
        self.assertEqual(
            constructor.return_value,
            make_attribute_manager(opts))
        constructor.assert_called_once_with(
            driver.return_value,
            opts.proxy,
            opts.target,
            opts.pvname,
            opts.pvinfo)
        driver.assert_called_once_with(opts.driver, opts.pvname)

    @mock.patch("epics_tango_bridge.manager_factory.make_driver_adapter")
    @mock.patch("epics_tango_bridge.manager_factory.CommandInManager")
    def test_should_create_command_in_manager_with_dummy_out(
            self,
            constructor,
            driver):
        self.create_command_in_manager(
            constructor,
            driver,
            PVINFO_ARG_VOID_OUT,
            is_dummy=True)

    @mock.patch("epics_tango_bridge.manager_factory.make_driver_adapter")
    @mock.patch("epics_tango_bridge.manager_factory.CommandInManager")
    def test_should_create_command_in_manager_with_concrete_out(
            self,
            constructor,
            driver):
        pvinfo = {}
        self.create_command_in_manager(
            constructor,
            driver,
            pvinfo,
            is_dummy=False)

    @mock.patch("epics_tango_bridge.manager_factory.make_driver_adapter")
    @mock.patch("epics_tango_bridge.manager_factory.CommandOutManager")
    def test_should_create_command_out_manager(self, constructor, driver):
        opts = mock.NonCallableMagicMock()
        self.assertEqual(
            constructor.return_value,
            make_command_out_manager(opts))
        constructor.assert_called_once_with(
            driver.return_value,
            opts.pvname)
        driver.assert_called_once_with(opts.driver, opts.pvname)
