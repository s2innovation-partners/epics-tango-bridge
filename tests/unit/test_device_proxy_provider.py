import unittest
import mock
from epics_tango_bridge.device_proxy_provider import DeviceProxyProvider


DEVICE_NAME = "some/test/device"
DEVICE_NAME_2 = "some/other/device"


class TestDeviceProxyProvider(unittest.TestCase):

    def setUp(self):
        self.out = DeviceProxyProvider()

    @mock.patch("PyTango.DeviceProxy")
    def test_should_provide_new_device_proxy_on_first_access(self, proxy):
        self.assertEqual(proxy.return_value, self.out(DEVICE_NAME))
        proxy.assert_called_once_with(DEVICE_NAME)

    @mock.patch("PyTango.DeviceProxy")
    def test_should_provide_cached_device_proxy_on_second_access(self, proxy):
        dev1 = mock.NonCallableMagicMock()
        dev2 = mock.NonCallableMagicMock()
        proxy.side_effect = [dev1, dev2]

        self.assertEqual(dev1, self.out(DEVICE_NAME))
        proxy.assert_called_once_with(DEVICE_NAME)

        self.assertEqual(dev1, self.out(DEVICE_NAME))
        proxy.assert_called_once_with(DEVICE_NAME)

        self.assertEqual(dev2, self.out(DEVICE_NAME_2))
        self.assertEqual(
            [mock.call(DEVICE_NAME), mock.call(DEVICE_NAME_2)],
            proxy.mock_calls)
