import unittest
import argparse
import logging

import mock

from epics_tango_bridge.bridge_server import run


OPTIONS = argparse.Namespace(
    log_level='WARNING',
    prefix="some_prefix",
    pvdb='path/to/pvdb.file')


class TestBridgeServer(unittest.TestCase):

    def assert_logging_configured(self, logging_config):  # noqa E501 pylint: disable=R0201
        logging_config.assert_called_once_with(
            format=mock.ANY,
            level=logging.WARNING)

    def assert_args_parsed(self, parser_factory):  # pylint: disable=R0201
        parser_factory.assert_called_once_with(description=mock.ANY)
        parser_factory.return_value.parse_args.assert_called_once_with()

    def assert_pvdb_loaded(self, run_path):  # pylint: disable=R0201
        run_path.assert_called_once_with(OPTIONS.pvdb)  # noqa E501 pylint: disable=no-member

    def set_args_parsing_result(self, parser_factory):  # pylint: disable=R0201
        parser = parser_factory.return_value
        parser.parse_args.return_value = OPTIONS

    @mock.patch("runpy.run_path")
    @mock.patch("logging.basicConfig")
    @mock.patch("argparse.ArgumentParser")
    @mock.patch("epics_tango_bridge.bridge_server.SignalHandler")
    @mock.patch("pcaspy.SimpleServer")
    @mock.patch("epics_tango_bridge.bridge_server.TangoDriver")
    def test_should_configure_all_services_and_run_the_server(  # noqa E501 pylint: disable=too-many-arguments
            self,
            driver_factory,
            server_factory,
            signal_factory,
            parser_factory,
            logging_config,
            run_path):
        STEPS = 5
        PVDB = object()
        server = server_factory.return_value
        signal = signal_factory.return_value
        self.set_args_parsing_result(parser_factory)
        run_path.return_value = {'pvdb': PVDB}
        type(signal).stop = mock.PropertyMock(
            side_effect=STEPS * [False] + [True])
        run()
        signal_factory.assert_called_once_with()
        server_factory.assert_called_once_with()
        driver_factory.assert_called_once_with(PVDB)
        server.createPV.assert_called_once_with(OPTIONS.prefix + ":", PVDB)  # noqa E501 pylint: disable=no-member
        self.assert_args_parsed(parser_factory)
        self.assert_logging_configured(logging_config)
        self.assert_pvdb_loaded(run_path)
        self.assertEqual(STEPS * [mock.call(0.1)], server.process.mock_calls)

    @mock.patch("runpy.run_path")
    @mock.patch("logging.basicConfig")
    @mock.patch("argparse.ArgumentParser")
    def test_should_return_when_pvdb_loading_fails(
            self,
            parser_factory,
            logging_config,
            run_path):
        self.set_args_parsing_result(parser_factory)
        run_path.side_effect = Exception
        run()
        self.assert_args_parsed(parser_factory)
        self.assert_logging_configured(logging_config)
        self.assert_pvdb_loaded(run_path)

    @mock.patch("runpy.run_path")
    @mock.patch("logging.basicConfig")
    @mock.patch("argparse.ArgumentParser")
    @mock.patch("epics_tango_bridge.bridge_server.SignalHandler")
    def test_should_return_server_initialization_fails_on_signal_handler(
            self,
            signal_factory,
            parser_factory,
            logging_config,
            run_path):
        self.set_args_parsing_result(parser_factory)
        signal_factory.side_effect = Exception
        run()
        self.assert_args_parsed(parser_factory)
        self.assert_logging_configured(logging_config)
        self.assert_pvdb_loaded(run_path)
        signal_factory.assert_called_once_with()
