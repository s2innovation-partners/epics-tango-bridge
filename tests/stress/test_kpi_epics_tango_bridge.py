import os
import statistics
import subprocess
import sys
import time
import unittest
import threading
import random
import string

import pcaspy.alarm
import epics
import epics.ca
import PyTango as tango
import pytest
from tests.generate_enviroment import generate_pvdb, generate_server  #pylint: disable=no-name-in-module,import-error

PYTHON_EXEC = "python3" if sys.version_info[0] >= 3 else "python2"

BRIDGE_IOC_EXEC = "epics-tango-bridge"
BRIDGE_IOC_PVDB_FILE = "./tests/stress/pvdb_kpi.py"

TANGO_SERVER_EXEC = "./tests/stress/server/EpicsTangoBridgeKPITestServer.py"
TANGO_SERVER_INST = "test_kpi"
TANGO_DEVICE = "test_kpi/epics_tango_bridge/1"

STEP_TEST = 100


def generate_all(pvsize, scan, type_variable, count_val=None):
    generate_pvdb(pvsize, BRIDGE_IOC_PVDB_FILE, scan, type_variable, count_val, TANGO_SERVER_INST)  # noqa: W292, E501 pylint: disable=C0301
    generate_server(pvsize, TANGO_SERVER_EXEC, type_variable, count_val)


def get_random_or_default_attribute():
    if pytest.RANDOM_ATTRIBUTE:
        tango_attr_test = "AttributeTest{}".format(random.randint(0, pytest.SIZE_PVDB_DATA - 1))  # noqa: W292, E501 pylint: disable=C0301,missing-format-argument-key
    else:
        tango_attr_test = "AttributeTest"

    return tango_attr_test, attr_pv_name(tango_attr_test)


def remove_outliers(data, num_deviations=3):
    if len(data) == 0:  #pylint: disable=len-as-condition
        return data
    avg = statistics.mean(data)
    allowed_range = statistics.stdev(data) * num_deviations
    min_val, max_val = avg - allowed_range, avg + allowed_range
    return [d for d in data if min_val <= d <= max_val]


def attr_pv_name(name):
    return "tango:{}:attr:{}".format(TANGO_DEVICE, name)


def cmd_in_name(name):
    return "tango:{}:cmd:{}:in".format(TANGO_DEVICE, name)


def cmd_out_name(name):
    return "tango:{}:cmd:{}:out".format(TANGO_DEVICE, name)


DEVNULL = open(os.devnull, 'w')


# Variable arrays in PCAS are available starting from 3.14.12.6
# see: https://epics.anl.gov/base/R3-14/12-docs/RELEASE_NOTES.html
# see: https://github.com/epics-extensions/ca-gateway/issues/16
HAS_VARIABLE_ARRAYS = (
    pcaspy.cas.EPICS_VERSION,
    pcaspy.cas.EPICS_REVISION,
    pcaspy.cas.EPICS_MODIFICATION,
    pcaspy.cas.EPICS_PATCH_LEVEL) >= (3, 14, 12, 6)


def build_ioc_env():
    env = os.environ.copy()
    env.pop('EPICS_CA_ADDR_LIST', None)
    env.pop('EPICS_CA_AUTO_ADDR_LIST', None)
    env['COVERAGE_PROCESS_START'] = ".coveragerc-integration"
    return env


def start_tango_server():
    return subprocess.Popen(
        args=[PYTHON_EXEC, TANGO_SERVER_EXEC, TANGO_SERVER_INST],
        stdout=DEVNULL,
        stderr=DEVNULL)


def start_epics_bridge_ioc():
    return subprocess.Popen(
        args=[BRIDGE_IOC_EXEC, BRIDGE_IOC_PVDB_FILE],
        stdout=DEVNULL,
        stderr=DEVNULL,
        env=build_ioc_env())


def clear_epics_global_pv_cache():
    from epics.pv import _PVcache_
    for pv in list(_PVcache_.values()):
        pv.disconnect()
    _PVcache_.clear()


def stop_process(proc):
    proc.terminate()
    guard = threading.Timer(5, proc.kill)
    guard.start()
    proc.wait()
    guard.cancel()


def get_pv_info(pv_name):
    pv = epics.PV(pv_name)
    pv.connect()
    try:
        info = lambda: None  # noqa E731
        for field in pv._fields:
            setattr(info, field, getattr(pv, field))
        return info
    finally:
        pv.disconnect()


def image_as_list(data):
    return list(map(list, data))


class TestKPIEpicsTangoBridge(unittest.TestCase):  # noqa E501 pylint: disable=too-many-public-methods

    @classmethod
    def setUpClass(cls):
        os.environ['EPICS_CA_ADDR_LIST'] = "127.0.0.1"
        os.environ['EPICS_CA_AUTO_ADDR_LIST'] = "NO"

    def start(self):
        time.sleep(1.0)
        self.proc_tango = start_tango_server()  #pylint: disable=W0201
        time.sleep(1.0)
        self.assertIsNone(self.proc_tango.poll())
        self.proc_ioc = start_epics_bridge_ioc()  #pylint: disable=W0201
        time.sleep(1.0)
        self.assertIsNone(self.proc_ioc.poll())
        self.proxy = tango.DeviceProxy(TANGO_DEVICE)  #pylint: disable=W0201

    def stop(self):
        try:
            self.assertIsNone(self.proc_ioc.poll())
            self.assertIsNone(self.proc_tango.poll())
        finally:
            clear_epics_global_pv_cache()
            self.stop_ioc()
            stop_process(self.proc_tango)

    def stop_ioc(self):
        epics.ca.finalize_libca()
        stop_process(self.proc_ioc)
        epics.ca.initialize_libca()

    def set_tango_attribute_config(  # pylint: disable=too-many-arguments
            self,
            attribute,
            unit='',
            min_warning='Not specified',
            max_warning='Not specified',
            min_alarm='Not specified',
            max_alarm='Not specified'):
        conf = self.proxy.get_attribute_config(attribute)
        conf.unit = unit
        conf.alarms.min_warning = min_warning
        conf.alarms.max_warning = max_warning
        conf.alarms.min_alarm = min_alarm
        conf.alarms.max_alarm = max_alarm
        self.proxy.set_attribute_config(conf)

    def test_should_check_max_number_of_double_attributes_in_bridge_with_tango_check(self):  # noqa: W292, E501 pylint: disable=C0301
        SIZE_PVDB_DATA = 8000
        VALID_SIZE_PVDB = None
        while(True):  #pylint: disable=C0325
            try:
                generate_all(SIZE_PVDB_DATA, 0, 'double')
                self.start()

                TANGO_ATTR_TEST = "AttributeTest{}".format(random.randint(0, SIZE_PVDB_DATA-1))  # noqa: W292, E501 pylint: disable=C0301,missing-format-argument-key
                self.proxy = tango.DeviceProxy(TANGO_DEVICE)  #pylint: disable=W0201
                val_rand = round(random.uniform(-200.0, 200.0), 2)
                self.proxy.write_attribute(TANGO_ATTR_TEST, val_rand)
                self.assertEqual(val_rand, self.proxy.read_attribute(TANGO_ATTR_TEST).value)  # noqa: W292, E501 pylint: disable=C0301
            except:  # noqa: E722 pylint: disable=W0702
                break
            finally:
                self.stop()
            VALID_SIZE_PVDB = SIZE_PVDB_DATA
            SIZE_PVDB_DATA += STEP_TEST

        print('\n')
        print("=======================================================================================================================================")  # noqa: W292, E501 pylint: disable=C0301
        print('The maximum size of the base in TANGO-EPICS-BRIDGE for the double parameter on this machine using tango commands is {}'.format(VALID_SIZE_PVDB))  # noqa: W292, E501 pylint: disable=C0301
        print("=======================================================================================================================================")  # noqa: W292, E501 pylint: disable=C0301
        print('\n')

    def test_should_check_max_number_of_string_attributes_in_bridge_with_tango_check(self):  # noqa: W292, E501 pylint: disable=C0301
        SIZE_PVDB_DATA = 8200
        VALID_SIZE_PVDB = None
        while(True):  #pylint: disable=C0325
            try:
                generate_all(SIZE_PVDB_DATA, 0, 'string')
                self.start()

                TANGO_ATTR_TEST = "AttributeTest{}".format(random.randint(0, SIZE_PVDB_DATA-1))  # noqa: W292, E501 pylint: disable=C0301
                self.proxy = tango.DeviceProxy(TANGO_DEVICE)  #pylint: disable=W0201
                val_rand = ''.join(random.choice(string.ascii_letters) for _ in range(20))  # noqa: W292, E501 pylint: disable=C0301
                self.proxy.write_attribute(TANGO_ATTR_TEST, val_rand)
                self.assertEqual(val_rand, self.proxy.read_attribute(TANGO_ATTR_TEST).value)  # noqa: W292, E501 pylint: disable=C0301

            except:  # noqa: E722 pylint: disable=W0702
                break
            finally:
                self.stop()
            VALID_SIZE_PVDB = SIZE_PVDB_DATA
            SIZE_PVDB_DATA += STEP_TEST

        print('\n')
        print("=======================================================================================================================================")  # noqa: W292, E501 pylint: disable=C0301
        print('The maximum size of the base in TANGO-EPICS-BRIDGE for the string parameter using tango commands on this machine is {}'.format(VALID_SIZE_PVDB))  # noqa: W292, E501 pylint: disable=C0301
        print("=======================================================================================================================================")  # noqa: W292, E501 pylint: disable=C0301
        print('\n')

    def test_should_check_max_number_of_double_spectrum_attributes_in_bridge_with_tango_check(self):  # noqa: W292, E501 pylint: disable=C0301
        for count in [100, 1000, 3000, 5000]:
            SIZE_PVDB_DATA = 8000
            VALID_SIZE_PVDB = None
            while(True):  #pylint: disable=C0325
                try:
                    generate_all(SIZE_PVDB_DATA, 0, 'double', count)

                    self.start()
                    TANGO_ATTR_TEST = "AttributeTest{}".format(random.randint(0, SIZE_PVDB_DATA-1))  # noqa: W292, E501 pylint: disable=C0301
                    self.proxy = tango.DeviceProxy(TANGO_DEVICE)  #pylint: disable=W0201
                    value = [11.32] * count
                    self.proxy.write_attribute(TANGO_ATTR_TEST, value)
                    self.assertEqual(list(self.proxy.read_attribute(TANGO_ATTR_TEST).value), value)  # noqa: W292, E501 pylint: disable=C0301
                except:  # noqa: E722 pylint: disable=W0702
                    break
                finally:
                    self.stop()

                VALID_SIZE_PVDB = SIZE_PVDB_DATA
                SIZE_PVDB_DATA += 100

            print('\n')
            print("=========================================================================================================================================================================")  # noqa: W292, E501 pylint: disable=C0301
            print('The maximum size of the base in TANGO-EPICS-BRIDGE for the double spectrum parameter with: {} elements on this machine using tango commands is {}'.format(count, VALID_SIZE_PVDB))  # noqa: W292, E501 pylint: disable=C0301
            print("=========================================================================================================================================================================")  # noqa: W292, E501 pylint: disable=C0301
            print('\n')

    def test_should_check_max_number_of_double_attributes_in_bridge_with_epics_check(self):  # noqa: W292, E501 pylint: disable=C0301
        SIZE_PVDB_DATA = 1500
        VALID_SIZE_PVDB = None
        while(True):  #pylint: disable=C0325
            try:
                generate_all(SIZE_PVDB_DATA, 0, 'double')
                self.start()

                TANGO_ATTR_TEST = "AttributeTest{}".format(random.randint(0, SIZE_PVDB_DATA-1))  # noqa: W292, E501 pylint: disable=C0301
                PV_ATTR_TEST = attr_pv_name(TANGO_ATTR_TEST)

                val_rand = round(random.uniform(-200.0, 200.0), 2)
                epics.caput(PV_ATTR_TEST, val_rand, wait=True)
                self.assertEqual(val_rand, epics.caget(PV_ATTR_TEST))
            except:  # noqa: E722 pylint: disable=W0702
                break
            finally:
                self.stop()
            VALID_SIZE_PVDB = SIZE_PVDB_DATA
            SIZE_PVDB_DATA += STEP_TEST

        print('\n')
        print("=======================================================================================================================================")  # noqa: W292, E501 pylint: disable=C0301
        print('The maximum size of the base in TANGO-EPICS-BRIDGE for the double parameter on this machine using epics commands is {}'.format(VALID_SIZE_PVDB))  # noqa: W292, E501 pylint: disable=C0301
        print("=======================================================================================================================================")  # noqa: W292, E501 pylint: disable=C0301
        print('\n')

    def test_should_check_max_number_of_string_attributes_in_bridge_with_epics_check(self):  # noqa: W292, E501 pylint: disable=C0301
        SIZE_PVDB_DATA = 1500
        VALID_SIZE_PVDB = None
        while(True):  #pylint: disable=C0325
            try:
                generate_all(SIZE_PVDB_DATA, 0, 'string')
                self.start()

                TANGO_ATTR_TEST = "AttributeTest{}".format(random.randint(0, SIZE_PVDB_DATA-1))  # noqa: W292, E501 pylint: disable=C0301
                PV_ATTR_TEST = attr_pv_name(TANGO_ATTR_TEST)

                val_rand = ''.join(random.choice(string.ascii_letters) for _ in range(20))  # noqa: W292, E501 pylint: disable=C0301
                epics.caput(PV_ATTR_TEST, val_rand, wait=True)
                self.assertEqual(val_rand, epics.caget(PV_ATTR_TEST))

            except:  # noqa: E722 pylint: disable=W0702
                break
            finally:
                self.stop()
            VALID_SIZE_PVDB = SIZE_PVDB_DATA
            SIZE_PVDB_DATA += STEP_TEST

        print('\n')
        print("=======================================================================================================================================")  # noqa: W292, E501 pylint: disable=C0301
        print('The maximum size of the base in TANGO-EPICS-BRIDGE for the string parameter using epics commands on this machine is {}'.format(VALID_SIZE_PVDB))  # noqa: W292, E501 pylint: disable=C0301
        print("=======================================================================================================================================")  # noqa: W292, E501 pylint: disable=C0301
        print('\n')

    def test_should_check_max_number_of_double_spectrum_attributes_in_bridge_with_epics_check(self):  # noqa: W292, E501 pylint: disable=C0301
        for count in [100, 1000, 3000, 5000]:
            SIZE_PVDB_DATA = 1200
            VALID_SIZE_PVDB = None
            while(True):  #pylint: disable=C0325
                try:
                    generate_all(SIZE_PVDB_DATA, 0, 'double', count)

                    self.start()
                    TANGO_ATTR_TEST = "AttributeTest{}".format(random.randint(0, SIZE_PVDB_DATA-1))  # noqa: W292, E501 pylint: disable=C0301
                    PV_ATTR_TEST = attr_pv_name(TANGO_ATTR_TEST)

                    val_rand = [round(random.uniform(-200.0, 200.0), 2) for _ in range(count)]  # noqa: W292, E501 pylint: disable=C0301
                    epics.caput(PV_ATTR_TEST, val_rand, wait=True)
                    self.assertEqual(val_rand, list(epics.caget(PV_ATTR_TEST)))
                except:  # noqa: E722 pylint: disable=W0702
                    break
                finally:
                    self.stop()

                VALID_SIZE_PVDB = SIZE_PVDB_DATA
                SIZE_PVDB_DATA += 100

            print('\n')
            print("=========================================================================================================================================================================")  # noqa: W292, E501 pylint: disable=C0301
            print('The maximum size of the base in TANGO-EPICS-BRIDGE for the double spectrum parameter with: {} elements on this machine using epics commands is {}'.format(count, VALID_SIZE_PVDB))  # noqa: W292, E501 pylint: disable=C0301
            print("=========================================================================================================================================================================")  # noqa: W292, E501 pylint: disable=C0301
            print('\n')
