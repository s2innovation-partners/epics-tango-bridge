import random
import os
import time
import statistics
import tango


time.sleep(10)
TANGO_DEVICE = "test/epics_tango_bridge/1"

def attr_pv_name(name):
    return "tango:{}:attr:{}".format(TANGO_DEVICE, name)  #pylint: disable=C0321

ATTR_DOUBLE_SCALAR = "DoubleScalar0"
PV_DOUBLE_SCALAR = attr_pv_name(ATTR_DOUBLE_SCALAR)

try:
    NUMBER_OF_OPERATIONS = int(os.environ['NUMBER_OF_OPERATIONS'])
except:  # noqa E722 pylint: disable=W0702
    NUMBER_OF_OPERATIONS = 200

print("NUMBER OF OPERATIONS set to {}".format(NUMBER_OF_OPERATIONS))

random_values = [round(random.uniform(-200.0, 200.0), 2) for _ in range(0, NUMBER_OF_OPERATIONS)]  # noqa E501 pylint: disable=C0301
read_values = []
read_times = []

proxy = tango.DeviceProxy(TANGO_DEVICE)

for i, value in enumerate(random_values):
    if i % 100 == 0:
        print("Number of reads: {}".format(i))
    start = time.time()
    val = proxy.read_attribute(ATTR_DOUBLE_SCALAR).value
    end = time.time()
    read_values.append(val)
    read_times.append(end-start)
    time.sleep(0.1)

print('\n')
print("===============================================================")  # noqa E501
print("Average read time from TANGO: {}".format(sum(read_times) / len(read_times)))  # noqa E501
print("Read time standard deviation: {}".format(statistics.stdev(read_times)))  # noqa E501
print("===============================================================")  # noqa E501
print('\n')
