use tango;

INSERT INTO device (name, domain, family, member, ior, server, class, comment) VALUES
    ('test/epics_tango_bridge/1',       'test',     'epics_tango_bridge',         '1',   NULL, 'EpicsTangoBridgeTestServer/test',   'EpicsTangoBridgeTestServer',   NULL),
    ('dserver/EpicsTangoBridgeTestServer/test',   'dserver', 'EpicsTangoBridgeTestServer',   'test',         NULL, 'EpicsTangoBridgeTestServer/test',   'DServer',      NULL);
