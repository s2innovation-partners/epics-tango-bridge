import os
import statistics
import subprocess
import sys
import time
import unittest
import threading
import random
import string

import pcaspy.alarm
import epics
import epics.ca
import PyTango as tango
import pytest

PYTHON_EXEC = "python3" if sys.version_info[0] >= 3 else "python2"

BRIDGE_IOC_EXEC = "epics-tango-bridge"
BRIDGE_IOC_PVDB_FILE = "./tests/stress/pvdb.py"

TANGO_SERVER_EXEC = "./tests/stress/server/EpicsTangoBridgeTestServer.py"
TANGO_SERVER_INST = "test"
TANGO_DEVICE = "test/epics_tango_bridge/1"
NUMBER_OF_OPERATIONS = 100


def get_random_or_default_attribute():
    if pytest.RANDOM_ATTRIBUTE:
        tango_attr_test = "AttributeTest{}".format(random.randint(0, pytest.SIZE_PVDB_DATA - 1))  # noqa E501 pylint: disable=C0301
    else:
        tango_attr_test = "AttributeTest"

    return tango_attr_test, attr_pv_name(tango_attr_test)


def get_random_list(type_variable):  # pylint: disable=R1710
    if type_variable is float:
        return [round(random.uniform(-200.0, 200.0), 2) for _ in range(0, NUMBER_OF_OPERATIONS)]  # noqa E501 pylint: disable=C0301
    if type_variable is str:
        return [''.join(random.choice(string.ascii_letters) for i in range(20)) for _ in range(0, NUMBER_OF_OPERATIONS)]  # noqa E501 pylint: disable=C0301


def remove_outliers(data, num_deviations=3):
    if len(data) == 0:  #pylint: disable=len-as-condition
        return data
    avg = statistics.mean(data)
    allowed_range = statistics.stdev(data) * num_deviations
    min_val, max_val = avg - allowed_range, avg + allowed_range
    return [d for d in data if min_val <= d <= max_val]


def attr_pv_name(name):
    return "tango:{}:attr:{}".format(TANGO_DEVICE, name)


def cmd_in_name(name):
    return "tango:{}:cmd:{}:in".format(TANGO_DEVICE, name)


def cmd_out_name(name):
    return "tango:{}:cmd:{}:out".format(TANGO_DEVICE, name)


DEVNULL = open(os.devnull, 'w')

# Variable arrays in PCAS are available starting from 3.14.12.6
# see: https://epics.anl.gov/base/R3-14/12-docs/RELEASE_NOTES.html
# see: https://github.com/epics-extensions/ca-gateway/issues/16
HAS_VARIABLE_ARRAYS = (
    pcaspy.cas.EPICS_VERSION,
    pcaspy.cas.EPICS_REVISION,
    pcaspy.cas.EPICS_MODIFICATION,
    pcaspy.cas.EPICS_PATCH_LEVEL) >= (3, 14, 12, 6)


def build_ioc_env():
    env = os.environ.copy()
    env.pop('EPICS_CA_ADDR_LIST', None)
    env.pop('EPICS_CA_AUTO_ADDR_LIST', None)
    env['COVERAGE_PROCESS_START'] = ".coveragerc-integration"
    return env


def start_tango_server():
    return subprocess.Popen(
        args=[PYTHON_EXEC, TANGO_SERVER_EXEC, TANGO_SERVER_INST],
        stdout=DEVNULL,
        stderr=DEVNULL)


def start_epics_bridge_ioc():
    return subprocess.Popen(
        args=[BRIDGE_IOC_EXEC, BRIDGE_IOC_PVDB_FILE],
        stdout=DEVNULL,
        stderr=DEVNULL,
        env=build_ioc_env())


def clear_epics_global_pv_cache():
    from epics.pv import _PVcache_
    for pv in list(_PVcache_.values()):
        pv.disconnect()
    _PVcache_.clear()


def stop_process(proc):
    proc.terminate()
    guard = threading.Timer(5, proc.kill)
    guard.start()
    proc.wait()
    guard.cancel()


def get_pv_info(pv_name):
    pv = epics.PV(pv_name)
    pv.connect()
    try:
        info = lambda: None  # noqa E731
        for field in pv._fields:
            setattr(info, field, getattr(pv, field))
        return info
    finally:
        pv.disconnect()


def image_as_list(data):
    return list(map(list, data))


class TestScenariosEpicsTangoBridge(unittest.TestCase):  # noqa E501 pylint: disable=too-many-public-methods

    @classmethod
    def setUpClass(cls):
        os.environ['EPICS_CA_ADDR_LIST'] = "127.0.0.1"
        os.environ['EPICS_CA_AUTO_ADDR_LIST'] = "NO"

    def setUp(self):
        time.sleep(1.0)
        self.proc_tango = start_tango_server()
        time.sleep(1.0)
        self.assertIsNone(self.proc_tango.poll())
        self.proc_ioc = start_epics_bridge_ioc()
        time.sleep(1.0)
        self.assertIsNone(self.proc_ioc.poll())
        self.proxy = tango.DeviceProxy(TANGO_DEVICE)

    def tearDown(self):
        try:
            self.assertIsNone(self.proc_ioc.poll())
            self.assertIsNone(self.proc_tango.poll())
        finally:
            clear_epics_global_pv_cache()
            self.stop_ioc()
            stop_process(self.proc_tango)

    def stop_ioc(self):
        epics.ca.finalize_libca()
        stop_process(self.proc_ioc)
        epics.ca.initialize_libca()

    def set_tango_attribute_config(  # pylint: disable=too-many-arguments
            self,
            attribute,
            unit='',
            min_warning='Not specified',
            max_warning='Not specified',
            min_alarm='Not specified',
            max_alarm='Not specified'):
        conf = self.proxy.get_attribute_config(attribute)
        conf.unit = unit
        conf.alarms.min_warning = min_warning
        conf.alarms.max_warning = max_warning
        conf.alarms.min_alarm = min_alarm
        conf.alarms.max_alarm = max_alarm
        self.proxy.set_attribute_config(conf)

    def test_should_check_response_time_of_read_attribute_from_epics(self):  # noqa: W292, E501
        _, PV_ATTR_TEST = get_random_or_default_attribute()
        type_variable = type(epics.caget(PV_ATTR_TEST))
        random_list = get_random_list(type_variable)
        read_values = []
        read_times = []

        for value in random_list:
            _, PV_ATTR_TEST = get_random_or_default_attribute()
            epics.caput(PV_ATTR_TEST, value, wait=True)

            start = time.time()
            val = epics.caget(PV_ATTR_TEST)
            end = time.time()
            read_values.append(val)
            read_times.append(end-start)

        self.assertEqual(len(read_times), NUMBER_OF_OPERATIONS)
        read_times = remove_outliers(read_times)
        print('\n')
        print("===============================================================")  # noqa: W292, E501 pylint: disable=C0301
        print("Average read time from EPICS: {}".format(sum(read_times) / len(read_times)))  # noqa: W292, E501 pylint: disable=C0301
        print("Read time standard deviation: {}".format(statistics.stdev(read_times)))  # noqa: W292, E501 pylint: disable=C0301
        print("===============================================================")  # noqa: W292, E501 pylint: disable=C0301
        print('\n')

    def test_should_check_response_time_of_read_attribute_from_tango(self):  # noqa: W292, E501 pylint: disable=C0301
        _, PV_ATTR_TEST = get_random_or_default_attribute()
        type_variable = type(epics.caget(PV_ATTR_TEST))
        random_list = get_random_list(type_variable)
        read_values = []
        read_times = []

        for value in random_list:
            TANGO_ATTR_TEST, PV_ATTR_TEST = get_random_or_default_attribute()  # noqa: W292, E501
            epics.caput(PV_ATTR_TEST, value, wait=True)
            start = time.time()
            val = self.proxy.read_attribute(TANGO_ATTR_TEST).value
            end = time.time()
            read_values.append(val)
            read_times.append(end-start)

        self.assertEqual(len(read_times), NUMBER_OF_OPERATIONS)
        read_times = remove_outliers(read_times)
        print('\n')
        print("===============================================================")  # noqa: W292, E501 pylint: disable=C0301
        print("Average read time from TANGO: {}".format(sum(read_times) / len(read_times)))  # noqa: W292, E501 pylint: disable=C0301
        print("Read time standard deviation: {}".format(statistics.stdev(read_times)))  # noqa: W292, E501 pylint: disable=C0301
        print("===============================================================")  # noqa: W292, E501 pylint: disable=C0301
        print('\n')

    def test_should_check_response_time_of_write_attribute_to_epics(self):  # noqa: W292, E501 pylint: disable=C0321
        def attr(attr_name): return self.proxy.read_attribute(attr_name).value  # noqa E501 pylint: disable=C0321

        _, PV_ATTR_TEST = get_random_or_default_attribute()
        type_variable = type(epics.caget(PV_ATTR_TEST))
        random_list = get_random_list(type_variable)
        read_values = []
        write_times = []

        for value in random_list:
            TANGO_ATTR_TEST, PV_ATTR_TEST = get_random_or_default_attribute()  # noqa: W292, E501

            start = time.time()
            epics.caput(PV_ATTR_TEST, value, wait=True)
            end = time.time()
            val = attr(TANGO_ATTR_TEST)

            read_values.append(val)
            write_times.append(end-start)

        self.assertEqual(len(write_times), NUMBER_OF_OPERATIONS)
        write_times = remove_outliers(write_times)
        print('\n')
        print("===============================================================")  # noqa: W292, E501 pylint: disable=C0301
        print("Average write time to EPICS: {}".format(sum(write_times) / len(write_times)))  # noqa: W292, E501 pylint: disable=C0301
        print("Write time standard deviation: {}".format(statistics.stdev(write_times)))  # noqa: W292, E501 pylint: disable=C0301
        print("===============================================================")  # noqa: W292, E501 pylint: disable=C0301
        print('\n')

    def test_should_check_response_time_of_write_attribute_to_tango(self):  # noqa: W292, E501
        def attr(attr_name): return self.proxy.read_attribute(attr_name).value  # noqa E501 pylint: disable=C0321

        _, PV_ATTR_TEST = get_random_or_default_attribute()
        type_variable = type(epics.caget(PV_ATTR_TEST))
        random_list = get_random_list(type_variable)
        read_values = []
        write_times = []

        for value in random_list:
            TANGO_ATTR_TEST, _ = get_random_or_default_attribute()

            start = time.time()
            self.proxy.write_attribute(TANGO_ATTR_TEST, value)
            end = time.time()
            val = attr(TANGO_ATTR_TEST)

            read_values.append(val)
            write_times.append(end-start)

        self.assertEqual(len(write_times), NUMBER_OF_OPERATIONS)
        write_times = remove_outliers(write_times)
        print('\n')
        print("===============================================================")  # noqa: W292, E501 pylint: disable=C0301
        print("Average write time to TANGO: {}".format(sum(write_times) / len(write_times)))  # noqa: W292, E501 pylint: disable=C0301
        print("Write time standard deviation: {}".format(statistics.stdev(write_times)))  # noqa: W292, E501 pylint: disable=C0301
        print("===============================================================")  # noqa: W292, E501 pylint: disable=C0301
        print('\n')
