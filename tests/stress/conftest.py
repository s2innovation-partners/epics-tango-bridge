import pytest
from tests import generate_enviroment  #pylint: disable=no-name-in-module


BRIDGE_IOC_EXEC = "epics-tango-bridge"
BRIDGE_IOC_PVDB_FILE = "./tests/stress/pvdb.py"

TANGO_SERVER_EXEC = "./tests/stress/server/EpicsTangoBridgeTestServer.py"
TANGO_SERVER_INST = "test"


def generate_all(pvsize, scan, type_variable):
    print("Generate a pvdb file and Tango server with base size: {} and type {}".format(pvsize, type_variable))  # noqa E501 pylint: disable=C0301
    generate_enviroment.generate_pvdb(pvsize, BRIDGE_IOC_PVDB_FILE, scan, type_variable)  # noqa E501 pylint: disable=C0301
    generate_enviroment.generate_server(pvsize, TANGO_SERVER_EXEC, type_variable)  # noqa E501 pylint: disable=C0301


def pytest_addoption(parser):
    parser.addoption("--setup", "--sC=", action="store", dest="setup", help="setup.")  # noqa E501 pylint: disable=C0301

    parser.addoption(
        "--pvdbsize",
        action="store",
        default="100",
        help="Size of PV DB: 1, 10, 100, 1000 or 5000",
        choices=("1", "10", "100", "1000", "5000"),
    )

    parser.addoption(
        "--scan",
        action="store",
        default="1",
        help="Scan: 0 or 1",
        choices=("0", "1"),
    )

    parser.addoption(
        "--type",
        action="store",
        default="string",
        help="Type of Attribute: double, string",
        choices=("string", "double"),
    )

    parser.addoption(
        "--randomattr",
        action="store",
        default="1",
        help="Read random attribute?",
        choices=("0", "1"),
    )


def pytest_configure(config):
    print("PV DB size: ", config.option.pvdbsize)
    print("Scan: ", config.option.scan)
    print("Read random attribute: ", config.option.randomattr)
    print("Type of Attributes in PV DB: ", config.option.type)

    pytest.SIZE_PVDB_DATA = int(config.option.pvdbsize)
    pytest.SCAN = int(config.option.scan)
    pytest.RANDOM_ATTRIBUTE = bool(int(config.option.randomattr))
    pytest.TYPE = str(config.option.type)
    generate_all(pytest.SIZE_PVDB_DATA, pytest.SCAN, pytest.TYPE)
