import sys
import PyTango as tango
import PyTango.server


tango.server = PyTango.server


if sys.version_info[0] >= 3:
    from six import add_metaclass as tango_add_metaclass
else:
    def tango_add_metaclass(_):
        def wrapper(cls):
            return cls
        return wrapper


@tango_add_metaclass(tango.server.DeviceMeta)
class EpicsTangoBridgeTestServer(tango.server.Device):

    __metaclass__ = tango.server.DeviceMeta

    DoubleScalar0 = tango.server.attribute(
        dtype='double',
        unit='dbsc',
        min_alarm='-200',
        max_alarm='200',
        min_warning='-100',
        max_warning='100',
        access=tango.AttrWriteType.READ_WRITE)
    fname = 'server/EpicsTangoBridgeTestServer.py'

    def init_device(self):
        tango.server.Device.init_device(self)
        self.set_state(tango.DevState.ON)

        self.__att_DoubleScalar0 = 0.0  #pylint: disable=W0201

    def read_DoubleScalar0(self):
        return self.__att_DoubleScalar0

    def write_DoubleScalar0(self, value):
        self.__att_DoubleScalar0 = value  #pylint: disable=W0201


if __name__ == '__main__':
    tango.server.run((EpicsTangoBridgeTestServer,))
