import setuptools


setuptools.setup(
    name="EpicsTangoBridgeTestServer",
    version="0.1",
    description="Simple device for testing EPICS Tango Bridge",
    author="S2Innovation",
    author_email="cpmtact@s2innovation.com",
    license="Proprietary",
    classifiers=[
        'License :: Other/Proprietary License'],
    packages=setuptools.find_packages(),
    install_requires=['configparser', 'fandango', 'lxml', 'jsonpointer', 'numpy', 'scipy==1.0.1', 'signals']  # noqa E501 pylint: disable=C0301
)
