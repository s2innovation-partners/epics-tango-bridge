import re


def generate_pvdb(size, filename, scan, type_var,  #pylint: disable=R0913
                  count_val=None, instance='test'):
    if type_var == 'double':
        type_var = 'float'
    keys = ["{}/epics_tango_bridge/1:attr:AttributeTest{}".format(instance, i) for i in range(size)]  #pylint: disable=C0301
    dict_tmp = {
        'type': type_var,
        'scan': scan,
        'asyn': True,
    }
    if count_val is not None:
        dict_tmp['count'] = count_val
    values = [dict_tmp] * size

    with open(filename, 'w') as f:
        f.write('import PyTango as tango\n')
        f.write('pvdb={}'.format(dict(zip(keys, values))))


def generate_server(size, filename, type_var, count_val=None):
    class_name = re.search('tests/stress/server/(.*).py', filename).group(1)
    server_template = """
import PyTango as tango
import PyTango.server
import sys


tango.server = PyTango.server


if sys.version_info[0] >= 3:
    from six import add_metaclass as tango_add_metaclass
else:
    def tango_add_metaclass(_):
        def wrapper(cls):
            return cls
        return wrapper


@tango_add_metaclass(tango.server.DeviceMeta)
class {}(tango.server.Device):

    __metaclass__ = tango.server.DeviceMeta
""".format(class_name)

    for i in range(size):
        server_template += """
    AttributeTest{} = tango.server.attribute(""".format(i)
        if count_val is not None:
            server_template += """
    dtype=('{}',),
    max_dim_x={},""".format(type_var, count_val)
        else:
            server_template += """
    dtype='{}',""".format(type_var)
        server_template += """
    access=tango.AttrWriteType.READ_WRITE)
"""
    server_template += """
    def init_device(self):
        tango.server.Device.init_device(self)
        self.set_state(tango.DevState.ON)
"""
    for i in range(size):
        server_template += """
        self.__att_AttributeTest{} = 0.0""".format(i)

    for i in range(size):
        server_template += """
    def read_AttributeTest{}(self):
        return self.__att_AttributeTest{}

    def write_AttributeTest{}(self, value):
        self.__att_AttributeTest{} = value
""".format(i, i, i, i)
    server_template += """
if __name__ == '__main__':
    tango.server.run(({},))
""".format(class_name)
    with open(filename, 'w') as f:
        f.write(server_template)
