import PyTango as tango
import PyTango.server
import sys


tango.server = PyTango.server


if sys.version_info[0] >= 3:
    from six import add_metaclass as tango_add_metaclass
else:
    def tango_add_metaclass(_):
        def wrapper(cls):
            return cls
        return wrapper


@tango_add_metaclass(tango.server.DeviceMeta)
class EpicsTangoBridgeTestServer(tango.server.Device):

    __metaclass__ = tango.server.DeviceMeta

    DoubleScalar = tango.server.attribute(
        dtype='double',
        unit='dbsc',
        min_alarm='-200',
        max_alarm='200',
        min_warning='-100',
        max_warning='100',
        access=tango.AttrWriteType.READ_WRITE)

    DoubleScalarTimesTwoCmdCallCount = tango.server.attribute(
        dtype='int',
        access=tango.AttrWriteType.READ)

    DoubleScalarTimesTwoCmdArgIn = tango.server.attribute(
        dtype='double',
        access=tango.AttrWriteType.READ)

    CmdDoubleScalar = tango.server.attribute(
        dtype='double',
        access=tango.AttrWriteType.READ_WRITE)

    PolledDoubleScalar = tango.server.attribute(
        dtype='double',
        polling_period=500,
        abs_change=10,
        access=tango.AttrWriteType.READ_WRITE)

    StringAttribute = tango.server.attribute(
        dtype='string',
        access=tango.AttrWriteType.READ_WRITE)

    StringAsCharsAttribute = tango.server.attribute(
        dtype='string',
        access=tango.AttrWriteType.READ_WRITE)

    DoubleSpectrum = tango.server.attribute(
        dtype=('double',),
        max_dim_x=128,
        access=tango.AttrWriteType.READ_WRITE)

    DoubleImage = tango.server.attribute(
        dtype=(('double',),),
        max_dim_x=16,
        max_dim_y=16,
        access=tango.AttrWriteType.READ_WRITE)

    def init_device(self):
        tango.server.Device.init_device(self)
        self.set_state(tango.DevState.ON)
        self.__att_DoubleScalar = 0.0
        self.__att_DoubleScalarTimesTwoCmdCallCount = 0
        self.__att_DoubleScalarTimesTwoCmdArgIn = 0.0
        self.__att_CmdDoubleScalar = 0.0
        self.__att_PolledDoubleScalar = 0.0
        self.__att_StringAttribute = ''
        self.__att_StringAsCharsAttribute = ''
        self.__att_DoubleSpectrum = []
        self.__att_DoubleImage = [[]]

    def read_DoubleScalar(self):
        return self.__att_DoubleScalar

    def write_DoubleScalar(self, value):
        self.__att_DoubleScalar = value

    def read_DoubleScalarTimesTwoCmdCallCount(self):
        return self.__att_DoubleScalarTimesTwoCmdCallCount

    def read_DoubleScalarTimesTwoCmdArgIn(self):
        return self.__att_DoubleScalarTimesTwoCmdArgIn

    def read_CmdDoubleScalar(self):
        return self.__att_CmdDoubleScalar

    def write_CmdDoubleScalar(self, value):
        self.__att_CmdDoubleScalar = value

    def read_PolledDoubleScalar(self):
        return self.__att_PolledDoubleScalar

    def write_PolledDoubleScalar(self, value):
        self.__att_PolledDoubleScalar = value

    def read_StringAttribute(self):
        return self.__att_StringAttribute

    def write_StringAttribute(self, value):
        self.__att_StringAttribute = value

    def read_StringAsCharsAttribute(self):
        return self.__att_StringAsCharsAttribute

    def write_StringAsCharsAttribute(self, value):
        self.__att_StringAsCharsAttribute = value

    def read_DoubleSpectrum(self):
        return self.__att_DoubleSpectrum

    def write_DoubleSpectrum(self, value):
        self.__att_DoubleSpectrum = value

    def read_DoubleImage(self):
        return self.__att_DoubleImage

    def write_DoubleImage(self, value):
        self.__att_DoubleImage = value

    @tango.server.command(dtype_in='double', dtype_out='double')
    def DoubleScalarTimesTwoCmd(self, argin):
        self.__att_DoubleScalarTimesTwoCmdCallCount += 1
        self.__att_DoubleScalarTimesTwoCmdArgIn = argin
        return 2.0 * argin

    @tango.server.command(dtype_out='double')
    def ReadDoubleScalarCmd(self):
        return self.__att_CmdDoubleScalar

    @tango.server.command(dtype_in='double')
    def WriteDoubleScalarCmd(self, argin):
        self.__att_CmdDoubleScalar = argin

    @tango.server.command(dtype_in='double')
    def WritePolledDoubleScalarCmd(self, argin):
        self.__att_PolledDoubleScalar = argin


if __name__ == '__main__':
    tango.server.run((EpicsTangoBridgeTestServer,))
