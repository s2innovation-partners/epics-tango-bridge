import PyTango as tango

pvdb = {
    "test/epics_tango_bridge/1:attr:DoubleScalar": {
        'type': 'float',
        'scan': 1,
        'asyn': True,
        'high': 100000.0,
        'hihi': 200000.0,
    },
    "test/epics_tango_bridge/1:attr:StringAttribute": {
        'type': 'string',
        'asyn': True,
    },
    "test/epics_tango_bridge/1:attr:StringAsCharsAttribute": {
        'type': 'char',
        'count': 128,
        'asyn': True,
    },
    "test/epics_tango_bridge/1:attr:State": {
        'type': 'enum',
        'enums': [name for _, name in sorted(
            [(k, v.name) for k, v in tango.DevState.values.items()])],
        'asyn': True,
    },
    "test/epics_tango_bridge/1:attr:Status": {
        'type': 'string',
        'asyn': True,
    },
    "test/epics_tango_bridge/1:attr:DoubleSpectrum": {
        'type': 'float',
        'count': 128,
        'asyn': True,
    },
    "test/epics_tango_bridge/1:attr:DoubleImage": {
        'type': 'float',
        'count': 256,
        'asyn': True,
    },
    "test/epics_tango_bridge/1:attr:NotExistingString": {
        'type': 'string',
        'asyn': True
    },
    "test/epics_tango_bridge/1:attr:PolledDoubleScalar": {
        'type': 'float',
        'asyn': True,
        '-tg-polled': True,
    },
    "test/epics_tango_bridge/1:cmd:DoubleScalarTimesTwoCmd:in": {
        'type': 'float',
        'asyn': True
    },
    "test/epics_tango_bridge/1:cmd:DoubleScalarTimesTwoCmd:out": {
        'type': 'float',
        'scan': 1,
    },
    "test/epics_tango_bridge/1:cmd:WriteDoubleScalarCmd:in": {
        'type': 'float',
        'asyn': True,
        '-tg-void-out': True
    },
    "test/epics_tango_bridge/1:cmd:ReadDoubleScalarCmd:in": {
        'asyn': True,
        '-tg-void-in': True,
    },
    "test/epics_tango_bridge/1:cmd:ReadDoubleScalarCmd:out": {
        'type': 'float',
        'scan': 1
    },
}
