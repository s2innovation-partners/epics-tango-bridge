import os
import subprocess
import sys
import time
import unittest
import threading
import itertools

import pcaspy
import pcaspy.alarm
import epics
import epics.ca
import PyTango as tango


PYTHON_EXEC = "python3" if sys.version_info[0] >= 3 else "python2"

BRIDGE_IOC_EXEC = "epics-tango-bridge"
BRIDGE_IOC_PVDB_FILE = "./tests/integration/pvdb.py"

TANGO_SERVER_EXEC = "./tests/integration/server/EpicsTangoBridgeTestServer.py"
TANGO_SERVER_INST = "test"
TANGO_DEVICE = "test/epics_tango_bridge/1"

ATTR_DOUBLE_SCALAR = "DoubleScalar"
ATTR_POLLED_DOUBLE_SCALAR = "PolledDoubleScalar"
ATTR_STRING = "StringAttribute"
ATTR_STRING_CHARS = "StringAsCharsAttribute"


def attr_pv_name(name):
    return "tango:{}:attr:{}".format(TANGO_DEVICE, name)


def cmd_in_name(name):
    return "tango:{}:cmd:{}:in".format(TANGO_DEVICE, name)


def cmd_out_name(name):
    return "tango:{}:cmd:{}:out".format(TANGO_DEVICE, name)


PV_DOUBLE_SCALAR = attr_pv_name(ATTR_DOUBLE_SCALAR)
PV_POLLED_DOUBLE_SCALAR = attr_pv_name(ATTR_POLLED_DOUBLE_SCALAR)
PV_STRING = attr_pv_name(ATTR_STRING)
PV_STRING_CHARS = attr_pv_name(ATTR_STRING_CHARS)
PV_DOUBLE_SPECTRUM = attr_pv_name("DoubleSpectrum")
PV_DOUBLE_IMAGE = attr_pv_name("DoubleImage")

PV_CMD_DOUBLE_DOUBLE_IN = cmd_in_name("DoubleScalarTimesTwoCmd")
PV_CMD_DOUBLE_DOUBLE_OUT = cmd_out_name("DoubleScalarTimesTwoCmd")

PV_CMD_VOID_DOUBLE_IN = cmd_in_name("ReadDoubleScalarCmd")
PV_CMD_VOID_DOUBLE_OUT = cmd_out_name("ReadDoubleScalarCmd")

PV_CMD_DOUBLE_VOID_IN = cmd_in_name("WriteDoubleScalarCmd")

DEVNULL = open(os.devnull, 'w')

# Variable arrays in PCAS are available starting from 3.14.12.6
# see: https://epics.anl.gov/base/R3-14/12-docs/RELEASE_NOTES.html
# see: https://github.com/epics-extensions/ca-gateway/issues/16
HAS_VARIABLE_ARRAYS = (
    pcaspy.cas.EPICS_VERSION,
    pcaspy.cas.EPICS_REVISION,
    pcaspy.cas.EPICS_MODIFICATION,
    pcaspy.cas.EPICS_PATCH_LEVEL) >= (3, 14, 12, 6)


def build_ioc_env():
    env = os.environ.copy()
    env.pop('EPICS_CA_ADDR_LIST', None)
    env.pop('EPICS_CA_AUTO_ADDR_LIST', None)
    env['COVERAGE_PROCESS_START'] = ".coveragerc-integration"
    return env


def start_tango_server():
    return subprocess.Popen(
        args=[PYTHON_EXEC, TANGO_SERVER_EXEC, TANGO_SERVER_INST],
        stdout=DEVNULL,
        stderr=DEVNULL)


def start_epics_bridge_ioc():
    return subprocess.Popen(
        args=[BRIDGE_IOC_EXEC, BRIDGE_IOC_PVDB_FILE],
        stdout=DEVNULL,
        stderr=DEVNULL,
        env=build_ioc_env())


def clear_epics_global_pv_cache():
    from epics.pv import _PVcache_
    for pv in list(_PVcache_.values()):
        pv.disconnect()
    _PVcache_.clear()


def stop_process(proc):
    proc.terminate()
    guard = threading.Timer(5, proc.kill)
    guard.start()
    proc.wait()
    guard.cancel()


def get_pv_info(pv_name):
    pv = epics.PV(pv_name)
    pv.connect()
    try:
        info = lambda: None  # noqa E731
        for field in pv._fields:
            setattr(info, field, getattr(pv, field))
        return info
    finally:
        pv.disconnect()


def image_as_list(data):
    return list(map(list, data))


class TestEpicsTangoBridge(unittest.TestCase):  # noqa E501 pylint: disable=too-many-public-methods

    @classmethod
    def setUpClass(cls):
        os.environ['EPICS_CA_ADDR_LIST'] = "127.0.0.1"
        os.environ['EPICS_CA_AUTO_ADDR_LIST'] = "NO"

    def setUp(self):
        self.proc_tango = start_tango_server()
        time.sleep(1.0)
        self.assertIsNone(self.proc_tango.poll())
        self.proc_ioc = start_epics_bridge_ioc()
        time.sleep(1.0)
        self.assertIsNone(self.proc_ioc.poll())
        self.proxy = tango.DeviceProxy(TANGO_DEVICE)

    def tearDown(self):
        try:
            self.assertIsNone(self.proc_ioc.poll())
            self.assertIsNone(self.proc_tango.poll())
        finally:
            clear_epics_global_pv_cache()
            self.stop_ioc()
            stop_process(self.proc_tango)

    def stop_ioc(self):
        epics.ca.finalize_libca()
        stop_process(self.proc_ioc)
        epics.ca.initialize_libca()

    def set_tango_attribute_config(  # pylint: disable=too-many-arguments
            self,
            attribute,
            unit='',
            min_warning='Not specified',
            max_warning='Not specified',
            min_alarm='Not specified',
            max_alarm='Not specified'):
        conf = self.proxy.get_attribute_config(attribute)
        conf.unit = unit
        conf.alarms.min_warning = min_warning
        conf.alarms.max_warning = max_warning
        conf.alarms.min_alarm = min_alarm
        conf.alarms.max_alarm = max_alarm
        self.proxy.set_attribute_config(conf)

    def test_should_read_double_scalar_attribute_from_tango(self):
        value = 3234.54
        def pv(): return epics.caget(PV_DOUBLE_SCALAR)  # noqa E501 pylint: disable=C0321
        self.assertNotEqual(pv(), value)
        self.proxy.write_attribute(ATTR_DOUBLE_SCALAR, value)
        time.sleep(2.0)
        self.assertEqual(pv(), value)

    def test_should_write_double_scalar_attribute_to_tango(self):
        value = 45322.16
        def attr(): return self.proxy.read_attribute(ATTR_DOUBLE_SCALAR).value  # noqa E501 pylint: disable=C0321
        self.assertNotEqual(attr(), value)
        epics.caput(PV_DOUBLE_SCALAR, value, wait=True)
        self.assertEqual(attr(), value)

    def test_should_read_string_attribute_from_tango(self):
        value = "hello world test"
        self.assertNotEqual(epics.caget(PV_STRING), value)
        self.proxy.write_attribute(ATTR_STRING, value)
        clear_epics_global_pv_cache()  # force count update
        self.assertEqual(epics.caget(PV_STRING), value)

    def test_should_write_string_attribute_to_tango(self):
        value = "my test string"
        self.assertNotEqual(self.proxy.StringAttribute, value)
        epics.caput(PV_STRING, value, wait=True)
        self.assertEqual(self.proxy.StringAttribute, value)

    def test_should_read_char_array_as_string_attribute_from_tango(self):
        value = ("13 characters"*10)[:127]
        def read_string_chars_pv():  # noqa E306
            return epics.caget(PV_STRING_CHARS, as_string=True, count=128)
        self.assertNotEqual(read_string_chars_pv(), value)
        self.proxy.write_attribute(ATTR_STRING_CHARS, value)
        clear_epics_global_pv_cache()  # force count update
        self.assertEqual(read_string_chars_pv(), value)

    def test_should_write_char_array_as_string_attribute_to_tango(self):
        value = ("my test string"*10)[:127]
        self.assertNotEqual(self.proxy.StringAsCharsAttribute, value)
        epics.caput(PV_STRING_CHARS, value, wait=True)
        self.assertEqual(self.proxy.StringAsCharsAttribute, value)

    def test_should_read_status_from_tango(self):
        status_pv = attr_pv_name("Status")
        self.assertEqual("The device is in ON state.", epics.caget(status_pv))

    def test_should_read_state_from_tango(self):
        state_pv = attr_pv_name("State")
        self.assertEqual("ON", epics.caget(state_pv, as_string=True))

    def test_should_write_double_spectrum_attribute_to_tango(self):
        value = [5, 6, 22, 11.32] * 20
        self.assertNotEqual(list(self.proxy.DoubleSpectrum), value)
        epics.caput(PV_DOUBLE_SPECTRUM, value, wait=True)
        self.assertEqual(list(self.proxy.DoubleSpectrum), value)

    def test_should_read_double_spectrum_attribute_from_tango(self):
        value = [22.0, 12.0, 4.0, 9.0, 5.0] * 17
        expected = value if HAS_VARIABLE_ARRAYS else (value + [0.0]*128)[:128]
        self.assertNotEqual(expected, list(epics.caget(PV_DOUBLE_SPECTRUM)))
        self.proxy.DoubleSpectrum = value
        clear_epics_global_pv_cache()  # force count update
        self.assertEqual(expected, list(epics.caget(PV_DOUBLE_SPECTRUM)))

    def test_should_write_double_image_attribute_to_tango(self):
        value = [[1.0]*10, [2.0]*10, [3.0]*10, [4.0]*10, [5.0]*10]
        flat = list(itertools.chain(*value))
        expected = [
            [1.0]*10 + [2.0]*6,
            [2.0]*4 + [3.0]*10 + [4.0]*2,
            [4.0]*8 + [5.0]*8,
            [5.0]*2 + [0.0]*14]
        self.assertNotEqual(expected, image_as_list(self.proxy.DoubleImage))
        epics.caput(PV_DOUBLE_IMAGE, flat, wait=True)
        self.assertEqual(expected, image_as_list(self.proxy.DoubleImage))

    def test_should_read_double_image_attribute_from_tango(self):
        value = [[33]*10, [22]*10, [55]*10]
        flat = list(itertools.chain(*value))
        expected = flat if HAS_VARIABLE_ARRAYS else (flat + [0.0]*256)[:256]
        self.assertNotEqual(expected, list(epics.caget(PV_DOUBLE_IMAGE)))
        self.proxy.DoubleImage = value
        clear_epics_global_pv_cache()  # force count update
        self.assertEqual(expected, list(epics.caget(PV_DOUBLE_IMAGE)))

    def test_should_write_double_image_shaped_as_last_read_value(self):
        put_value = [1, 2, 3, 4, 5]
        old_shaped = [put_value + [0]*11]
        new_shaped = [put_value[:2], put_value[2:4], put_value[4:] + [0]]
        epics.caput(PV_DOUBLE_IMAGE, put_value, wait=True)
        self.assertEqual(old_shaped, image_as_list(self.proxy.DoubleImage))
        self.proxy.DoubleImage = [[7, 8], [9, 6]]
        clear_epics_global_pv_cache()  # force count update
        write_as_flat = [7, 8, 9, 6]
        write_as_flat = write_as_flat if HAS_VARIABLE_ARRAYS \
            else (write_as_flat + [0.0]*256)[:256]
        self.assertEqual(write_as_flat, list(epics.caget(PV_DOUBLE_IMAGE)))
        epics.caput(PV_DOUBLE_IMAGE, put_value, wait=True)
        self.assertEqual(new_shaped, image_as_list(self.proxy.DoubleImage))

    def test_should_execute_command_with_argument_and_with_result(self):
        argin = 341.20
        self.assertEqual(0, self.proxy.DoubleScalarTimesTwoCmdCallCount)
        epics.caput(PV_CMD_DOUBLE_DOUBLE_IN, argin, wait=True)
        self.assertEqual(1, self.proxy.DoubleScalarTimesTwoCmdCallCount)
        self.assertEqual(argin, self.proxy.DoubleScalarTimesTwoCmdArgIn)
        self.assertEqual(2 * argin, epics.caget(PV_CMD_DOUBLE_DOUBLE_OUT))

    def test_should_execute_command_with_void_argument(self):
        value = 31344.127
        epics.caput(PV_CMD_VOID_DOUBLE_IN, 0, wait=True)
        self.assertEqual(0, epics.caget(PV_CMD_VOID_DOUBLE_OUT))
        self.proxy.CmdDoubleScalar = value
        epics.caput(PV_CMD_VOID_DOUBLE_IN, 0, wait=True)
        time.sleep(2)
        self.assertEqual(value, epics.caget(PV_CMD_VOID_DOUBLE_OUT))

    def test_should_execute_command_with_void_result(self):
        value = 45212.673
        self.assertEqual(0, self.proxy.CmdDoubleScalar)
        epics.caput(PV_CMD_DOUBLE_VOID_IN, value, wait=True)
        self.assertEqual(value, self.proxy.CmdDoubleScalar)

    def test_should_read_polled_double_scalar(self):
        abs_change = 10
        poll_period = 0.5
        def pv(): return epics.caget(PV_POLLED_DOUBLE_SCALAR)  # noqa E501 pylint: disable=C0321
        self.assertEqual(0, self.proxy.PolledDoubleScalar)
        self.proxy.command_inout("WritePolledDoubleScalarCmd", abs_change - 1)
        time.sleep(2 * poll_period)
        self.assertEqual(0, pv())
        self.proxy.command_inout("WritePolledDoubleScalarCmd", abs_change)
        time.sleep(2 * poll_period)
        self.assertEqual(abs_change, pv())

    def test_should_read_unit_and_limits_from_tango_if_not_set_in_pvdb(self):
        info = get_pv_info(PV_DOUBLE_SCALAR)
        self.assertEqual(pcaspy.alarm.Alarm.NO_ALARM, info.status)
        self.assertEqual(pcaspy.alarm.Severity.NO_ALARM, info.severity)
        self.assertEqual('dbsc', info.units)
        self.assertEqual(-200.0, info.lower_alarm_limit)
        self.assertEqual(-100.0, info.lower_warning_limit)
        self.assertEqual(200000.0, info.upper_alarm_limit)
        self.assertEqual(100000.0, info.upper_warning_limit)

    def test_should_not_read_unit_and_limits_if_values_are_not_in_tango(self):
        info = get_pv_info(PV_POLLED_DOUBLE_SCALAR)
        self.assertEqual(pcaspy.alarm.Alarm.NO_ALARM, info.status)
        self.assertEqual(pcaspy.alarm.Severity.NO_ALARM, info.severity)
        self.assertEqual('', info.units)
        self.assertEqual(0.0, info.lower_alarm_limit)
        self.assertEqual(0.0, info.lower_warning_limit)
        self.assertEqual(0.0, info.upper_alarm_limit)
        self.assertEqual(0.0, info.upper_warning_limit)

    def test_should_update_unit_and_limits_from_tango_at_runtime(self):
        self.set_tango_attribute_config(ATTR_POLLED_DOUBLE_SCALAR)

        min_alarm = 3.0
        min_warning = 4.0
        max_alarm = 6.0
        max_warning = 5.0

        self.set_tango_attribute_config(
            ATTR_POLLED_DOUBLE_SCALAR,
            unit='my_unit',
            min_alarm=str(min_alarm),
            min_warning=str(min_warning),
            max_alarm=str(max_alarm),
            max_warning=str(max_warning))

        info = get_pv_info(PV_POLLED_DOUBLE_SCALAR)
        self.assertEqual('my_unit', info.units)
        self.assertEqual(min_alarm, info.lower_alarm_limit)
        self.assertEqual(min_warning, info.lower_warning_limit)
        self.assertEqual(max_alarm, info.upper_alarm_limit)
        self.assertEqual(max_warning, info.upper_warning_limit)

        self.set_tango_attribute_config(ATTR_POLLED_DOUBLE_SCALAR)
        info = get_pv_info(PV_POLLED_DOUBLE_SCALAR)
        self.assertEqual('', info.units)
        self.assertEqual(0.0, info.lower_alarm_limit)
        self.assertEqual(0.0, info.lower_warning_limit)
        self.assertEqual(0.0, info.upper_alarm_limit)
        self.assertEqual(0.0, info.upper_warning_limit)

    def test_should_set_status_and_severity_according_to_thresholds(self):
        self.set_tango_attribute_config(ATTR_POLLED_DOUBLE_SCALAR)

        poll_period = 0.5

        self.proxy.PolledDoubleScalar = 100
        time.sleep(2 * poll_period)
        info = get_pv_info(PV_POLLED_DOUBLE_SCALAR)
        self.assertEqual(pcaspy.alarm.Alarm.NO_ALARM, info.status)
        self.assertEqual(pcaspy.alarm.Severity.NO_ALARM, info.severity)

        self.set_tango_attribute_config(
            ATTR_POLLED_DOUBLE_SCALAR,
            max_warning='20',
            max_alarm='50')
        time.sleep(2 * poll_period)
        info = get_pv_info(PV_POLLED_DOUBLE_SCALAR)
        self.assertEqual(pcaspy.alarm.Alarm.HIHI_ALARM, info.status)
        self.assertEqual(pcaspy.alarm.Severity.MAJOR_ALARM, info.severity)

        self.set_tango_attribute_config(
            ATTR_POLLED_DOUBLE_SCALAR,
            max_warning='20',
            max_alarm='150')
        time.sleep(2 * poll_period)
        info = get_pv_info(PV_POLLED_DOUBLE_SCALAR)
        self.assertEqual(pcaspy.alarm.Alarm.HIGH_ALARM, info.status)
        self.assertEqual(pcaspy.alarm.Severity.MINOR_ALARM, info.severity)

        self.set_tango_attribute_config(ATTR_POLLED_DOUBLE_SCALAR)
