# epics-tango-bridge

Access Tango device servers via EPICS' Channel Access.

## Documentation
Please find documentation on [readthedocs](https://epics-tango-bridge.readthedocs.io/en/latest/).

## Installation

The Epics to Tango Bridge IOC can be installed with `pip` from source tree:

```bash
pip install .
```

Following dependencies are required:
* [`pytango`](https://pypi.org/project/pytango/) (tested with 9.3.4 (libtango 9.3.4))
* [`pcaspy`](https://pypi.org/project/pcaspy/) (tested with 0.7.3 (epics-base 3.15.9 and 7.0.5))

Note: PCAS library is not any more distributed in EPICS base. According to [installation guide of pcaspy](https://pcaspy.readthedocs.io/en/latest/installation.html#getting-epics) (library used by epics-tango-bridge) it is suggested to simplify the build process by building PCAS library together with EPICS base using following steps:
* download source from https://github.com/epics-modules/pcas/releases
* unpack content of that to <EPICS_BASE>/modules/pcaspy
* add following content to <EPICS_BASE>/modules/Makefile.local (if file does not exist, create it):

        SUBMODULES += pcas
        pcas_DEPEND_DIRS = libcom


## Usage

Use `epics-tango-bridge` command to start Bridge IOC.

```
usage: epics-tango-bridge [-h] [-V] [--log-level LVL] [--prefix PREFIX] PVDB

Access Tango device servers via EPICS' Channel Access

positional arguments:
  PVDB             load PVs from PVDB file

optional arguments:
  -h, --help       show this help message and exit
  -V, --version    show program's version number and exit
  -W, --warranty    show details of Disclaimer of Warranty
  -C, --conditions  shows details under which conditions can be redistributed
  --log-level LVL  set logging level to LVL (default INFO)
  --prefix PREFIX  use PREFIX to prefix PV names (default tango)
```

Mandatory argument *PVDB* must be a path to a PVDB file. Example PVDB file:

```python
device = "sys/tg_test/1"
database = "sys/database/2"
pvdb = {
    f"{device}:attr:ampli": {
        "type": "float",
        "scan": 1,
        "asyn": True,
    },
    f"{database}:cmd:DbGetDeviceWideList:in": {
        "type": "string",
        "asyn": True,
    },
    f"{database}:cmd:DbGetDeviceWideList:out": {
        "type": "string",
        "count": 64,
    },
}
```

First, start the Bridge IOC:
```
$ epics-tango-bridge ./path/to/pvdb.py
```

To read an attribute, use `caget`:
```
$ caget -c 'tango:sys/tg_test/1:attr:ampli'
tango:sys/tg_test/1:attr:ampli 36
```

To write an attribute, use `caput`:
```
$ caput -c 'tango:sys/tg_test/1:attr:ampli' 15
Old : tango:sys/tg_test/1:attr:ampli 36
New : tango:sys/tg_test/1:attr:ampli 15
```

To run a command, use `caput` to set input PV and then use `caget`
to read output PV:
```
$ cacmd() { caput -c -w5 "$1:in" "$2"; caget -c "$1:out"; }
$ cacmd 'tango:sys/database/2:cmd:DbGetDeviceWideList' 'sys/benchmark/*'
Old : tango:sys/database/2:cmd:DbGetDeviceWideList:in sys/*
New : tango:sys/database/2:cmd:DbGetDeviceWideList:in sys/benchmark/*
tango:sys/database/2:cmd:DbGetDeviceWideList:out 2 sys/benchmark/javatarget01 sys/benchmark/pytarget01
```

## PVDB configuration

*PVDB* file defines mapping between EPICS' PVs and Tango device attributes
and commands. It is executed as a Python module and must export `pdvb`
variable. The structure of `pvdb` variable and allowed configuration options
are described in
[pcaspy documentation](https://pcaspy.readthedocs.io/en/latest/api.html#pcaspy.SimpleServer.createPV).

See [example PVDB file used for integration tests](tests/integration/pvdb.py).

### Attributes

A PV mapped to an attribute must be named as follows:
```python
f"{device_name}:attr:{attribute_name}"
```

#### Image attributes

If the Tango attribute is a two-dimensional list, it will be flattened during
read from the PV, i.e. `[[1,2],[3,4]]` will be read as `[1,2,3,4]`.

During write to the PV, the Bridge IOC will attempt to restore the correct
structure before passing the data to Tango. Either `dim_x` from last Tango
read (if available) or `max_dim_x` will be used to transform flat list
`[6,7,8,9]` back into `[[6,7],[8,9]]`. Last sub-list will be padded with
trailing zeros if needed.

#### Polling and events

Additional option `-tg-polled` can be set to `True` to indicate that attribute
is polled by a Tango device server. The Bridge IOC will attempt to subscribe
to the `CHANGE_EVENT` for such attribute.

Note: `scan` option should not be set when `-tg-polled` is used.

#### Device state

Device state can be read as an enum attribute. To get textual representation
of enumerated states, provide mapping in `enums` option.

Note: it is possible to extract state to string mapping from `pytango`:

```python
"test/epics_tango_bridge/1:attr:State": {
    'type': 'enum',
    'enums': [name for _, name in sorted([(k, v.name) for k, v in tango.DevState.values.items()])],
},
```

#### Units and alarms

The Bridge IOC will attempt to read `unit`, `low`, `high`, `lolo`, `hihi`
from Tango to configure unit and alarm thresholds if no value is set for these
options in PVDB. To disable this behavior, values (possibly default) must be
specified explicitly in PVDB.


### Commands

Command can be mapped to two PVs:
```python
f"{device_name}:cmd:{command_name}:in"   # PV for input
f"{device_name}:cmd:{command_name}:out"  # PV for output (read-only, optional)
```

Command is executed during write to the `:in` PV, with value of that PV used
as an input argument.

After write is completed, command execution result is stored in the `:out` PV.

#### Commands with no arguments

Option `-tg-void-in` must be set to `True` on `:in` PV if a command requires
no arguments (input is `DevVoid`). `type` option can be omitted for such PV
(and will default to `float`). Write of any value will trigger command
execution.

#### Commands with no result

Option `-tg-void-out` must be set to `True` on `:in` PV if a command produces
no result (output is `DevVoid`). `:out` PV will never be updated and may not
be defined in PVDB.

## Limitations

Types that are sequence of two elements (like `DevVarLongStringArray`)
are not supported.

## Contributors 
Please check [Contributors](./CONTRIBUTORS).

## License 
Please check [License](./LICENSE)
