
PYTHON ?= python

lint:
	$(PYTHON) setup.py flake8

pylint:
	pylint epics_tango_bridge
	pylint --disable=invalid-name --min-similarity-lines=9 tests

ut:
	$(PYTHON) setup.py test --test-suite tests.unit

utcov:
	coverage run \
		--branch \
		--source epics_tango_bridge \
		setup.py test \
		--test-suite tests.unit
	coverage report \
		--show-missing \
		--fail-under=100

int:
	$(PYTHON) setup.py test --test-suite tests.integration

intcov:
	$(eval TMP := $(shell mktemp -d))
	echo 'import coverage; coverage.process_startup()' > $(TMP)/sitecustomize.py
	PYTHONPATH=$(TMP) $(PYTHON) setup.py test \
		--test-suite tests.integration
	coverage combine
	coverage report \
		--show-missing \
		--fail-under=50
	rm -rf $(TMP)

test:
	$(PYTHON) setup.py test --test-suite tests.integration.test_epics_tango_bridge.TestEpicsTangoBridge.$(TEST)

.PHONY: lint pylint ut utcov int intcov test
