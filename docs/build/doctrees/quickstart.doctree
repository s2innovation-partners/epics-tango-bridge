��`]      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Installation�h]�h �Text����Installation�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�</home/tango-cs/epics-tango-bridge/docs/source/quickstart.rst�hKubh �	paragraph���)��}�(h�&Before installation run these commands�h]�h�&Before installation run these commands�����}�(hh/hh-hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh �enumerated_list���)��}�(hhh]�(h �	list_item���)��}�(h�Run ``sudo apt update``�h]�h,)��}�(hhDh]�(h�Run �����}�(h�Run �hhFubh �literal���)��}�(h�``sudo apt update``�h]�h�sudo apt update�����}�(hhhhPubah}�(h]�h!]�h#]�h%]�h']�uh)hNhhFubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhBubah}�(h]�h!]�h#]�h%]�h']�uh)h@hh=hhhh*hNubhA)��}�(h�Run ``sudo apt install swig``
�h]�h,)��}�(h�Run ``sudo apt install swig``�h]�(h�Run �����}�(h�Run �hhnubhO)��}�(h�``sudo apt install swig``�h]�h�sudo apt install swig�����}�(hhhhwubah}�(h]�h!]�h#]�h%]�h']�uh)hNhhnubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhjubah}�(h]�h!]�h#]�h%]�h']�uh)h@hh=hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']��enumtype��arabic��prefix�h�suffix��.�uh)h;hhhhhh*hKubh,)��}�(h�oAssuming you have Python already, The EPICS to Tango Bridge IOC can be installed with ``pip`` from source tree:�h]�(h�VAssuming you have Python already, The EPICS to Tango Bridge IOC can be installed with �����}�(h�VAssuming you have Python already, The EPICS to Tango Bridge IOC can be installed with �hh�hhhNhNubhO)��}�(h�``pip``�h]�h�pip�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)hNhh�ubh� from source tree:�����}�(h� from source tree:�hh�hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK	hhhhubh<)��}�(hhh]�(hA)��}�(h�KClone repository ``https://gitlab.com/s2innovation/epics-tango-bridge.git``�h]�h,)��}�(hh�h]�(h�Clone repository �����}�(h�Clone repository �hh�ubhO)��}�(h�:``https://gitlab.com/s2innovation/epics-tango-bridge.git``�h]�h�6https://gitlab.com/s2innovation/epics-tango-bridge.git�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)hNhh�ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h@hh�hhhh*hNubhA)��}�(h�]Install with ``pip``

 .. code-block:: console

    cd epics-tango-bridge
    pip install .

�h]�(h,)��}�(h�Install with ``pip``�h]�(h�Install with �����}�(h�Install with �hh�ubhO)��}�(h�``pip``�h]�h�pip�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)hNhh�ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�ubh �block_quote���)��}�(hhh]�h �literal_block���)��}�(h�#cd epics-tango-bridge
pip install .�h]�h�#cd epics-tango-bridge
pip install .�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve��language��console��linenos���highlight_args�}�uh)j  hh*hKhj
  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hh�ubeh}�(h]�h!]�h#]�h%]�h']�uh)h@hh�hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�h�hh�h�uh)h;hhhhhh*hKubh,)��}�(h�L**Note that for using EPICS Tango Bridge it is required to have installed:**�h]�h �strong���)��}�(hj8  h]�h�HNote that for using EPICS Tango Bridge it is required to have installed:�����}�(hhhj<  ubah}�(h]�h!]�h#]�h%]�h']�uh)j:  hj6  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh<)��}�(hhh]�(hA)��}�(h��**Tango Controls** (installation: https://tango-controls.readthedocs.io/en/latest/installation/index.html). EPICS Tango bridge was tested with
Tango 9.3.�h]�h,)��}�(h��**Tango Controls** (installation: https://tango-controls.readthedocs.io/en/latest/installation/index.html). EPICS Tango bridge was tested with
Tango 9.3.�h]�(j;  )��}�(h�**Tango Controls**�h]�h�Tango Controls�����}�(hhhjZ  ubah}�(h]�h!]�h#]�h%]�h']�uh)j:  hjV  ubh� (installation: �����}�(h� (installation: �hjV  ubh �	reference���)��}�(h�Ghttps://tango-controls.readthedocs.io/en/latest/installation/index.html�h]�h�Ghttps://tango-controls.readthedocs.io/en/latest/installation/index.html�����}�(hhhjo  ubah}�(h]�h!]�h#]�h%]�h']��refuri�jq  uh)jm  hjV  ubh�0). EPICS Tango bridge was tested with
Tango 9.3.�����}�(h�0). EPICS Tango bridge was tested with
Tango 9.3.�hjV  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhjR  ubah}�(h]�h!]�h#]�h%]�h']�uh)h@hjO  hhhh*hNubhA)��}�(h��**EPICS Controls** (installation: https://epics-controls.org/resources-and-support/documents/getting-started/).
EPICS Tango bridge was tested with EPICS 3.15 and EPICS 7.�h]�h,)��}�(h��**EPICS Controls** (installation: https://epics-controls.org/resources-and-support/documents/getting-started/).
EPICS Tango bridge was tested with EPICS 3.15 and EPICS 7.�h]�(j;  )��}�(h�**EPICS Controls**�h]�h�EPICS Controls�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j:  hj�  ubh� (installation: �����}�(h� (installation: �hj�  ubjn  )��}�(h�Khttps://epics-controls.org/resources-and-support/documents/getting-started/�h]�h�Khttps://epics-controls.org/resources-and-support/documents/getting-started/�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']��refuri�j�  uh)jm  hj�  ubh�=).
EPICS Tango bridge was tested with EPICS 3.15 and EPICS 7.�����}�(h�=).
EPICS Tango bridge was tested with EPICS 3.15 and EPICS 7.�hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h@hjO  hhhh*hNubhA)��}�(hX�  **PCAS** - Portable Channel Access Server
library with a C++ library with a simple class interface.
Using the interface to that library user can create a Channel Access server tool that can
interact with the EPICS database as well as other applications
Note that from PCAS library is not any more distributed in EPICS base.
According to `installation guide of pcaspy <https://pcaspy.readthedocs.io/en/latest/installation.html#getting-epics>`_
(library used by epics-tango-bridge) it is suggested to simplify the build process by building PCAS library together with EPICS base
using following steps:

- download source from https://github.com/epics-modules/pcas/releases
- unpack content of that to ``<EPICS_BASE>/modules/pcaspy``
- add following content to ``<EPICS_BASE>/modules/Makefile.local`` (if file does not exist, create it):

.. code-block:: console

    SUBMODULES += pcas
    pcas_DEPEND_DIRS = libcom

- run ``make``
�h]�(h,)��}�(hXV  **PCAS** - Portable Channel Access Server
library with a C++ library with a simple class interface.
Using the interface to that library user can create a Channel Access server tool that can
interact with the EPICS database as well as other applications
Note that from PCAS library is not any more distributed in EPICS base.
According to `installation guide of pcaspy <https://pcaspy.readthedocs.io/en/latest/installation.html#getting-epics>`_
(library used by epics-tango-bridge) it is suggested to simplify the build process by building PCAS library together with EPICS base
using following steps:�h]�(j;  )��}�(h�**PCAS**�h]�h�PCAS�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j:  hj�  ubhXI   - Portable Channel Access Server
library with a C++ library with a simple class interface.
Using the interface to that library user can create a Channel Access server tool that can
interact with the EPICS database as well as other applications
Note that from PCAS library is not any more distributed in EPICS base.
According to �����}�(hXI   - Portable Channel Access Server
library with a C++ library with a simple class interface.
Using the interface to that library user can create a Channel Access server tool that can
interact with the EPICS database as well as other applications
Note that from PCAS library is not any more distributed in EPICS base.
According to �hj�  ubjn  )��}�(h�i`installation guide of pcaspy <https://pcaspy.readthedocs.io/en/latest/installation.html#getting-epics>`_�h]�h�installation guide of pcaspy�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']��name��installation guide of pcaspy��refuri��Ghttps://pcaspy.readthedocs.io/en/latest/installation.html#getting-epics�uh)jm  hj�  ubh �target���)��}�(h�J <https://pcaspy.readthedocs.io/en/latest/installation.html#getting-epics>�h]�h}�(h]��installation-guide-of-pcaspy�ah!]�h#]��installation guide of pcaspy�ah%]�h']��refuri�j�  uh)j�  �
referenced�Khj�  ubh��
(library used by epics-tango-bridge) it is suggested to simplify the build process by building PCAS library together with EPICS base
using following steps:�����}�(h��
(library used by epics-tango-bridge) it is suggested to simplify the build process by building PCAS library together with EPICS base
using following steps:�hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhj�  ubh �bullet_list���)��}�(hhh]�(hA)��}�(h�Cdownload source from https://github.com/epics-modules/pcas/releases�h]�h,)��}�(hj  h]�(h�download source from �����}�(h�download source from �hj  ubjn  )��}�(h�.https://github.com/epics-modules/pcas/releases�h]�h�.https://github.com/epics-modules/pcas/releases�����}�(hhhj#  ubah}�(h]�h!]�h#]�h%]�h']��refuri�j%  uh)jm  hj  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK#hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h@hj  ubhA)��}�(h�9unpack content of that to ``<EPICS_BASE>/modules/pcaspy``�h]�h,)��}�(hj@  h]�(h�unpack content of that to �����}�(h�unpack content of that to �hjB  ubhO)��}�(h�``<EPICS_BASE>/modules/pcaspy``�h]�h�<EPICS_BASE>/modules/pcaspy�����}�(hhhjJ  ubah}�(h]�h!]�h#]�h%]�h']�uh)hNhjB  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK$hj>  ubah}�(h]�h!]�h#]�h%]�h']�uh)h@hj  ubhA)��}�(h�fadd following content to ``<EPICS_BASE>/modules/Makefile.local`` (if file does not exist, create it):
�h]�h,)��}�(h�eadd following content to ``<EPICS_BASE>/modules/Makefile.local`` (if file does not exist, create it):�h]�(h�add following content to �����}�(h�add following content to �hjh  ubhO)��}�(h�'``<EPICS_BASE>/modules/Makefile.local``�h]�h�#<EPICS_BASE>/modules/Makefile.local�����}�(hhhjq  ubah}�(h]�h!]�h#]�h%]�h']�uh)hNhjh  ubh�% (if file does not exist, create it):�����}�(h�% (if file does not exist, create it):�hjh  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK%hjd  ubah}�(h]�h!]�h#]�h%]�h']�uh)h@hj  ubeh}�(h]�h!]�h#]�h%]�h']��bullet��-�uh)j  hh*hK#hj�  ubj  )��}�(h�,SUBMODULES += pcas
pcas_DEPEND_DIRS = libcom�h]�h�,SUBMODULES += pcas
pcas_DEPEND_DIRS = libcom�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�j  j  j  �console�j!  �j"  }�uh)j  hh*hK'hj�  ubj  )��}�(hhh]�hA)��}�(h�run ``make``
�h]�h,)��}�(h�run ``make``�h]�(h�run �����}�(h�run �hj�  ubhO)��}�(h�``make``�h]�h�make�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hNhj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK,hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h@hj�  ubah}�(h]�h!]�h#]�h%]�h']�j�  j�  uh)j  hh*hK,hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h@hjO  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�h�h�h�hh�h�uh)h;hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�Usage�h]�h�Usage�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK/ubh,)��}�(h�7Use ``epics-tango-bridge`` command to start Bridge IOC.�h]�(h�Use �����}�(h�Use �hj�  hhhNhNubhO)��}�(h�``epics-tango-bridge``�h]�h�epics-tango-bridge�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hNhj�  ubh� command to start Bridge IOC.�����}�(h� command to start Bridge IOC.�hj�  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK0hj�  hhubj  )��}�(hXL  usage: epics-tango-bridge [-h] [-V] [--log-level LVL] [--prefix PREFIX] PVDB

Access Tango device servers via EPICS' Channel Access

positional arguments:
  PVDB             load PVs from PVDB file

optional arguments:
  -h, --help       show this help message and exit
  -V, --version    show program's version number and exit
  -W, --warranty    show details of Disclaimer of Warranty
  -C, --conditions  shows details under which conditions can be redistributed
  --log-level LVL  set logging level to LVL (default INFO)
  --prefix PREFIX  use PREFIX to prefix PV names (default tango)�h]�hXL  usage: epics-tango-bridge [-h] [-V] [--log-level LVL] [--prefix PREFIX] PVDB

Access Tango device servers via EPICS' Channel Access

positional arguments:
  PVDB             load PVs from PVDB file

optional arguments:
  -h, --help       show this help message and exit
  -V, --version    show program's version number and exit
  -W, --warranty    show details of Disclaimer of Warranty
  -C, --conditions  shows details under which conditions can be redistributed
  --log-level LVL  set logging level to LVL (default INFO)
  --prefix PREFIX  use PREFIX to prefix PV names (default tango)�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�j  j  j  �console�j!  �j"  }�uh)j  hh*hK2hj�  hhubh,)��}�(h�:The mandatory argument PVDB must be a path to a PVDB file.�h]�h�:The mandatory argument PVDB must be a path to a PVDB file.�����}�(hj)  hj'  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKChj�  hhubeh}�(h]��usage�ah!]�h#]��usage�ah%]�h']�uh)h	hhhhhh*hK/ubh
)��}�(hhh]�(h)��}�(h�How to use EPICS TANGO Bridge?�h]�h�How to use EPICS TANGO Bridge?�����}�(hjB  hj@  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj=  hhhh*hKGubh,)��}�(h�Start the bridge IOC:�h]�h�Start the bridge IOC:�����}�(hjP  hjN  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKHhj=  hhubj  )��}�(h�&$ epics-tango-bridge ./path/to/pvdb.py�h]�h�&$ epics-tango-bridge ./path/to/pvdb.py�����}�(hhhj\  ubah}�(h]�h!]�h#]�h%]�h']�j  j  j  �console�j!  �j"  }�uh)j  hh*hKJhj=  hhubh,)��}�(hXi  PVDB file defines mapping between EPICS' PVs and Tango device attributes
and commands. It is executed as a Python module and must export ``pdvb``
variable. The structure of ``pvdb`` variable and allowed configuration options
are described in
`pcaspy <https://pcaspy.readthedocs.io/en/latest/api.html?highlight=pvdb#pcaspy.SimpleServer.createPV>`_ documentation.�h]�(h��PVDB file defines mapping between EPICS’ PVs and Tango device attributes
and commands. It is executed as a Python module and must export �����}�(h��PVDB file defines mapping between EPICS' PVs and Tango device attributes
and commands. It is executed as a Python module and must export �hjl  hhhNhNubhO)��}�(h�``pdvb``�h]�h�pdvb�����}�(hhhju  ubah}�(h]�h!]�h#]�h%]�h']�uh)hNhjl  ubh�
variable. The structure of �����}�(h�
variable. The structure of �hjl  hhhNhNubhO)��}�(h�``pvdb``�h]�h�pvdb�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hNhjl  ubh�= variable and allowed configuration options
are described in
�����}�(h�= variable and allowed configuration options
are described in
�hjl  hhhNhNubjn  )��}�(h�h`pcaspy <https://pcaspy.readthedocs.io/en/latest/api.html?highlight=pvdb#pcaspy.SimpleServer.createPV>`_�h]�h�pcaspy�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']��name��pcaspy�j�  �\https://pcaspy.readthedocs.io/en/latest/api.html?highlight=pvdb#pcaspy.SimpleServer.createPV�uh)jm  hjl  ubj�  )��}�(h�_ <https://pcaspy.readthedocs.io/en/latest/api.html?highlight=pvdb#pcaspy.SimpleServer.createPV>�h]�h}�(h]��pcaspy�ah!]�h#]��pcaspy�ah%]�h']��refuri�j�  uh)j�  j  Khjl  ubh� documentation.�����}�(h� documentation.�hjl  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKNhj=  hhubj  )��}�(hX1  import PyTango as tango
device = "test/epics_tango_bridge/1"

pvdb = {
    f"${device}:attr:IntScalar": {
        'type': 'int',
        'scan': 1,
        'asyn': True,
    },
    f"${device}:attr:DoubleScalar": {
        'type': 'float',
        'scan': 1,
        'asyn': True,
        'high': 100000.0,
        'hihi': 200000.0,
    },
    f"${device}:attr:StringAttribute": {
        'type': 'string',
        'asyn': True,
    },
    f"${device}:attr:StringAsCharsAttribute": {
        'type': 'char',
        'count': 128,
        'asyn': True,
    },
    f"${device}:attr:State": {
        'type': 'enum',
        'enums': [name for _, name in sorted(
            [(k, v.name) for k, v in tango.DevState.values.items()])],
        'asyn': True,
    },
    f"${device}:attr:Status": {
        'type': 'string',
        'asyn': True,
    },
    f"${device}:attr:DoubleSpectrum": {
        'type': 'float',
        'count': 128,
        'asyn': True,
    },
    f"${device}:attr:DoubleImage": {
        'type': 'float',
        'count': 256,
        'asyn': True,
    },
    f"${device}:attr:NotExistingString": {
        'type': 'string',
        'asyn': True
    },
    f"${device}:attr:PolledDoubleScalar": {
        'type': 'float',
        'asyn': True,
        '-tg-polled': True,
    },
    f"${device}:cmd:DoubleScalarTimesTwoCmd:in": {
        'type': 'float',
        'asyn': True
    },
    f"${device}:cmd:DoubleScalarTimesTwoCmd:out": {
        'type': 'float',
        'scan': 1,
    },
    f"${device}:cmd:WriteDoubleScalarCmd:in": {
        'type': 'float',
        'asyn': True,
        '-tg-void-out': True
    },
    f"${device}:cmd:ReadDoubleScalarCmd:in": {
        'asyn': True,
        '-tg-void-in': True,
    },
    f"${device}:cmd:ReadDoubleScalarCmd:out": {
        'type': 'float',
        'scan': 1
    },
}�h]�hX1  import PyTango as tango
device = "test/epics_tango_bridge/1"

pvdb = {
    f"${device}:attr:IntScalar": {
        'type': 'int',
        'scan': 1,
        'asyn': True,
    },
    f"${device}:attr:DoubleScalar": {
        'type': 'float',
        'scan': 1,
        'asyn': True,
        'high': 100000.0,
        'hihi': 200000.0,
    },
    f"${device}:attr:StringAttribute": {
        'type': 'string',
        'asyn': True,
    },
    f"${device}:attr:StringAsCharsAttribute": {
        'type': 'char',
        'count': 128,
        'asyn': True,
    },
    f"${device}:attr:State": {
        'type': 'enum',
        'enums': [name for _, name in sorted(
            [(k, v.name) for k, v in tango.DevState.values.items()])],
        'asyn': True,
    },
    f"${device}:attr:Status": {
        'type': 'string',
        'asyn': True,
    },
    f"${device}:attr:DoubleSpectrum": {
        'type': 'float',
        'count': 128,
        'asyn': True,
    },
    f"${device}:attr:DoubleImage": {
        'type': 'float',
        'count': 256,
        'asyn': True,
    },
    f"${device}:attr:NotExistingString": {
        'type': 'string',
        'asyn': True
    },
    f"${device}:attr:PolledDoubleScalar": {
        'type': 'float',
        'asyn': True,
        '-tg-polled': True,
    },
    f"${device}:cmd:DoubleScalarTimesTwoCmd:in": {
        'type': 'float',
        'asyn': True
    },
    f"${device}:cmd:DoubleScalarTimesTwoCmd:out": {
        'type': 'float',
        'scan': 1,
    },
    f"${device}:cmd:WriteDoubleScalarCmd:in": {
        'type': 'float',
        'asyn': True,
        '-tg-void-out': True
    },
    f"${device}:cmd:ReadDoubleScalarCmd:in": {
        'asyn': True,
        '-tg-void-in': True,
    },
    f"${device}:cmd:ReadDoubleScalarCmd:out": {
        'type': 'float',
        'scan': 1
    },
}�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�j  j  j  �python�j!  �j"  }�uh)j  hh*hKThj=  hhubh,)��}�(h�#To read an attribute use ``caget``:�h]�(h�To read an attribute use �����}�(h�To read an attribute use �hj�  hhhNhNubhO)��}�(h�	``caget``�h]�h�caget�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hNhj�  ubh�:�����}�(h�:�hj�  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj=  hhubj  )��}�(h�+$ caget -c 'tango:sys/tg_test/1:attr:ampli'�h]�h�+$ caget -c 'tango:sys/tg_test/1:attr:ampli'�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�j  j  j  �console�j!  �j"  }�uh)j  hh*hK�hj=  hhubh,)��}�(h�%To write an attribute, use ``caput``:�h]�(h�To write an attribute, use �����}�(h�To write an attribute, use �hj  hhhNhNubhO)��}�(h�	``caput``�h]�h�caput�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)hNhj  ubh�:�����}�(hj�  hj  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj=  hhubj  )��}�(h�~$ caput -c 'tango:sys/tg_test/1:attr:ampli' 15
Old : tango:sys/tg_test/1:attr:ampli 36
New : tango:sys/tg_test/1:attr:ampli 15�h]�h�~$ caput -c 'tango:sys/tg_test/1:attr:ampli' 15
Old : tango:sys/tg_test/1:attr:ampli 36
New : tango:sys/tg_test/1:attr:ampli 15�����}�(hhhj'  ubah}�(h]�h!]�h#]�h%]�h']�j  j  j  �console�j!  �j"  }�uh)j  hh*hK�hj=  hhubh,)��}�(h�QTo run a command, use caput to set input PV and then use caget
to read output PV:�h]�h�QTo run a command, use caput to set input PV and then use caget
to read output PV:�����}�(hj9  hj7  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj=  hhubj  )��}�(hXm  $ cacmd() { caput -c -w5 "$1:in" "$2"; caget -c "$1:out"; }
$ cacmd 'tango:sys/database/2:cmd:DbGetDeviceWideList' 'sys/benchmark/*'
Old : tango:sys/database/2:cmd:DbGetDeviceWideList:in sys/*
New : tango:sys/database/2:cmd:DbGetDeviceWideList:in sys/benchmark/*
tango:sys/database/2:cmd:DbGetDeviceWideList:out 2 sys/benchmark/javatarget01 sys/benchmark/pytarget01�h]�hXm  $ cacmd() { caput -c -w5 "$1:in" "$2"; caget -c "$1:out"; }
$ cacmd 'tango:sys/database/2:cmd:DbGetDeviceWideList' 'sys/benchmark/*'
Old : tango:sys/database/2:cmd:DbGetDeviceWideList:in sys/*
New : tango:sys/database/2:cmd:DbGetDeviceWideList:in sys/benchmark/*
tango:sys/database/2:cmd:DbGetDeviceWideList:out 2 sys/benchmark/javatarget01 sys/benchmark/pytarget01�����}�(hhhjE  ubah}�(h]�h!]�h#]�h%]�h']�j  j  j  �console�j!  �j"  }�uh)j  hh*hK�hj=  hhubeh}�(h]��how-to-use-epics-tango-bridge�ah!]�h#]��how to use epics tango bridge?�ah%]�h']�uh)h	hhhhhh*hKGubeh}�(h]��installation�ah!]�h#]��installation�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(jb  j_  j  j�  j:  j7  jZ  jW  j�  j�  u�	nametypes�}�(jb  Nj  �j:  NjZ  Nj�  �uh}�(j_  hj�  j�  j7  j�  jW  j=  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.