.. epics-tango-bridge documentation master file, created by
   sphinx-quickstart on Tue May 31 12:01:29 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to epics-tango-bridge's documentation!
==============================================

EPICS Tango Bridge is a tool which allow to access
Tango device servers via EPICS' Channel Access.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart
   configuration
   contributors
   license

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
