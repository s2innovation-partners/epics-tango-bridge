==================
Installation
==================
Before installation run these commands

1. Run ``sudo apt update``
2. Run ``sudo apt install swig``

Assuming you have Python already, The EPICS to Tango Bridge IOC can be installed with ``pip`` from source tree:

1. Clone repository ``https://gitlab.com/s2innovation-partners/epics-tango-bridge.git``
2. Install with ``pip``

    .. code-block:: console

       cd epics-tango-bridge
       pip install .


**Note that for using EPICS Tango Bridge it is required to have installed:**

1.  **Tango Controls** (installation: https://tango-controls.readthedocs.io/en/latest/installation/index.html). EPICS Tango bridge was tested with
    Tango 9.3.
2.  **EPICS Controls** (installation: https://epics-controls.org/resources-and-support/documents/getting-started/).
    EPICS Tango bridge was tested with EPICS 3.15 and EPICS 7.
3.  **PCAS** - Portable Channel Access Server
    library with a C++ library with a simple class interface.
    Using the interface to that library user can create a Channel Access server tool that can
    interact with the EPICS database as well as other applications
    Note that from PCAS library is not any more distributed in EPICS base.
    According to `installation guide of pcaspy <https://pcaspy.readthedocs.io/en/latest/installation.html#getting-epics>`_
    (library used by epics-tango-bridge) it is suggested to simplify the build process by building PCAS library together with EPICS base
    using following steps:

    - download source from https://github.com/epics-modules/pcas/releases
    - unpack content of that to ``<EPICS_BASE>/modules/pcaspy``
    - add following content to ``<EPICS_BASE>/modules/Makefile.local`` (if file does not exist, create it):

    .. code-block:: console

        SUBMODULES += pcas
        pcas_DEPEND_DIRS = libcom

    - run ``make``

Usage
==================
Use ``epics-tango-bridge`` command to start Bridge IOC.

.. code-block:: console

    usage: epics-tango-bridge [-h] [-V] [--log-level LVL] [--prefix PREFIX] PVDB

    Access Tango device servers via EPICS' Channel Access

    positional arguments:
      PVDB             load PVs from PVDB file

    optional arguments:
      -h, --help       show this help message and exit
      -V, --version    show program's version number and exit
      -W, --warranty    show details of Disclaimer of Warranty
      -C, --conditions  shows details under which conditions can be redistributed
      --log-level LVL  set logging level to LVL (default INFO)
      --prefix PREFIX  use PREFIX to prefix PV names (default tango)

The mandatory argument PVDB must be a path to a PVDB file.


How to use EPICS TANGO Bridge?
=================================
Start the bridge IOC:

.. code-block:: console

   $ epics-tango-bridge ./path/to/pvdb.py

PVDB file defines mapping between EPICS' PVs and Tango device attributes
and commands. It is executed as a Python module and must export ``pdvb``
variable. The structure of ``pvdb`` variable and allowed configuration options
are described in
`pcaspy <https://pcaspy.readthedocs.io/en/latest/api.html?highlight=pvdb#pcaspy.SimpleServer.createPV>`_ documentation.

Here is an example of pvdb with different types of attributes:

.. code-block:: python

    import PyTango as tango

    pvdb = {
        "test/epics_tango_bridge/1:attr:IntScalar": {
            'type': 'int',
            'scan': 1,
            'asyn': True,
        },
        "test/epics_tango_bridge/1:attr:DoubleScalar": {
            'type': 'float',
            'scan': 1,
            'asyn': True,
            'high': 100000.0,
            'hihi': 200000.0,
        },
        "test/epics_tango_bridge/1:attr:StringAttribute": {
            'type': 'string',
            'asyn': True,
        },
        "test/epics_tango_bridge/1:attr:StringAsCharsAttribute": {
            'type': 'char',
            'count': 128,
            'asyn': True,
        },
        "test/epics_tango_bridge/1:attr:State": {
            'type': 'enum',
            'enums': [name for _, name in sorted(
                [(k, v.name) for k, v in tango.DevState.values.items()])],
            'asyn': True,
        },
        "test/epics_tango_bridge/1:attr:Status": {
            'type': 'string',
            'asyn': True,
        },
        "test/epics_tango_bridge/1:attr:DoubleSpectrum": {
            'type': 'float',
            'count': 128,
            'asyn': True,
        },
        "test/epics_tango_bridge/1:attr:DoubleImage": {
            'type': 'float',
            'count': 256,
            'asyn': True,
        },
        "test/epics_tango_bridge/1:attr:PolledDoubleScalar": {
            'type': 'float',
            'asyn': True,
            '-tg-polled': True,
        },
        "test/epics_tango_bridge/1:cmd:DoubleScalarTimesTwoCmd:in": {
            'type': 'float',
            'asyn': True
        },
        "test/epics_tango_bridge/1:cmd:DoubleScalarTimesTwoCmd:out": {
            'type': 'float',
            'scan': 1,
        },
        "test/epics_tango_bridge/1:cmd:WriteDoubleScalarCmd:in": {
            'type': 'float',
            'asyn': True,
            '-tg-void-out': True
        },
        "test/epics_tango_bridge/1:cmd:ReadDoubleScalarCmd:in": {
            'asyn': True,
            '-tg-void-in': True,
        },
        "test/epics_tango_bridge/1:cmd:ReadDoubleScalarCmd:out": {
            'type': 'float',
            'scan': 1
        },
    }



To read an attribute use ``caget``:

.. code-block:: console

   $ caget -c 'tango:sys/tg_test/1:attr:ampli'

To write an attribute, use ``caput``:

.. code-block:: console

   $ caput -c 'tango:sys/tg_test/1:attr:ampli' 15
   Old : tango:sys/tg_test/1:attr:ampli 36
   New : tango:sys/tg_test/1:attr:ampli 15


To run a command, use caput to set input PV and then use caget
to read output PV:

.. code-block:: console

    $ cacmd() { caput -c -w5 "$1:in" "$2"; caget -c "$1:out"; }
    $ cacmd 'tango:sys/database/2:cmd:DbGetDeviceWideList' 'sys/benchmark/*'
    Old : tango:sys/database/2:cmd:DbGetDeviceWideList:in sys/*
    New : tango:sys/database/2:cmd:DbGetDeviceWideList:in sys/benchmark/*
    tango:sys/database/2:cmd:DbGetDeviceWideList:out 2 sys/benchmark/javatarget01 sys/benchmark/pytarget01
