==================
Configuration
==================

The idea of EPICS Tango Bridge is to provide mapping between EPICS PVs and Tango device
attributes and commands. That mapping is described in `PVDB` file.

``PVDB`` executed as a Python module and must export ``pdvb``
variable.
In `PVDB` file there must be exported ``pvdb`` variable, which is Python's dictionary. It defines PVs which will be created in IOC database.
In general pvdb assumes following format:

.. code-block:: console

   pvdb = {
     'base_name' : {
        'field_name' : value,
      },
   }

The base_name is unique and will be prefixed to create PV full name.
The ``field_name`` is used to configure the PV properties. Table of
possible fields with descriptions and default values can be found in
`pcaspy documentation <https://pcaspy.readthedocs.io/en/latest/api.html#pcaspy.SimpleServer.createPV>`_.
In EPICS Tango Bridge the naming convention is more detailed to create mapping EPICS and Tango.


Attributes
----------
In Tango each device name is a three fields name. The field simulator is the / character. A Tango device name
looks like ``domain/family/member``. In EPICS there is a convention of using : as a field separator, but there are no strict
requirements about PV names as in Tango device names.
A PV mapped to an attribute must be named as follows:

.. code-block:: console

   f"{device_name}:attr:{attribute_name}"

Example of attribute in ``pvdb``:

.. code-block:: console

   "{device_name}:attr:DoubleScalar": {
       'type': 'float',
       'scan': 1,
       'asyn': True,
       'high': 100000.0,
       'hihi': 200000.0,
   }


Image attributes
~~~~~~~~~~~~~~~~~~

If the Tango attribute is a two-dimensional list, it will be flattened during
read from the PV, i.e. ``[[1,2],[3,4]]`` will be read as ``[1,2,3,4]``.
During write to the PV, the Bridge IOC will attempt to restore the correct
structure before passing the data to Tango. Either dim_x from last Tango
read (if available) or ``max_dim_x`` will be used to transform flat list
``[6,7,8,9]`` back into ``[[6,7],[8,9]]``. Last sub-list will be padded with
trailing zeros if needed.

Example of image attribute in ``pvdb``:

.. code-block:: console

   "{device_name}:attr:DoubleImage": {
     'type': 'float',
     'count': 256,
     'asyn': True,
   }


Polling and events
~~~~~~~~~~~~~~~~~~

Additional option ``-tg-polled`` can be set to ``True`` to indicate that attribute
is polled by a Tango device server. The Bridge IOC will attempt to subscribe
to the CHANGE_EVENT for such attribute.
Note: scan option should not be set when ``-tg-polled`` is used.

Example of polled attribute in ``pvdb``:

.. code-block:: console

   "{device_name}:attr:PolledDoubleScalar": {
     'type': 'float',
     'asyn': True,
     '-tg-polled': True,
   }

Device state
~~~~~~~~~~~~

Device state can be read as an enum attribute. To get textual representation
of enumerated states, provide mapping in `enums` field.
Note: it is possible to extract state to string mapping from `pytango`:

Example of enum attribute in ``pvdb``:

.. code-block:: console

   "test/epics_tango_bridge/1:attr:State": {
       'type': 'enum',
       'enums': [name for _, name in sorted([(k, v.name) for k, v in tango.DevState.values.items()])],
   }

Units and alarms
~~~~~~~~~~~~~~~~
The Bridge IOC will attempt to read `unit`, `low`, `high`, `lolo`, `hihi`
from Tango to configure unit and alarm thresholds if no value is set for these
options in PVDB. To disable this behavior, values (possibly default) must be
specified explicitly in PVDB.

Example of attribute with alarm threasholds in ``pvdb``.

.. code-block:: console

   "{device_name}:attr:DoubleScalar": {
       'type': 'float',
       'scan': 1,
       'asyn': True,
       'high': 100000.0,
       'hihi': 200000.0,
   }

Commands
--------
Command can be mapped to two PVs:

.. code-block:: console

   f"{device_name}:cmd:{command_name}:in"   # PV for input
   f"{device_name}:cmd:{command_name}:out"  # PV for output (read-only, optional)

Command is executed during write to the :in PV, with value of that PV used
as an input argument.
After write is completed, command execution result is stored in the :out PV.

Example of command in ``pvdb``:

.. code-block:: console

   "{device_name}:cmd:DoubleScalarCmd:in": {
       'type': 'float',
       'asyn': True
   },
   "{device_name}:cmd:DoubleScalarCmd:out": {
       'type': 'float',
       'scan': 1,
   }


Commands with no arguments
~~~~~~~~~~~~~~~~~~~~~~~~~~
Option ``-tg-void-in`` must be set to True on :in PV if a command requires
no arguments (input is ``DevVoid``). type option can be omitted for such PV
(and will default to float). Write of any value will trigger command
execution.

Example of command in with no arguments:

.. code-block:: console

   "test/epics_tango_bridge/1:cmd:ReadDoubleScalarCmd:in": {
       'asyn': True,
       '-tg-void-in': True,
   },


Commands with no result
~~~~~~~~~~~~~~~~~~~~~~~~
Option ``-tg-void-out`` must be set to True on :in PV if a command produces
no result (output is DevVoid). :out PV will never be updated and may not
be defined in `PVDB`.

Example of command in with no result:

.. code-block:: console

   "test/epics_tango_bridge/1:cmd:WriteDoubleScalarCmd:in": {
       'type': 'float',
       'asyn': True,
       '-tg-void-out': True
   },

Limitations
-----------
Types that are sequence of two elements (like ``DevVarLongStringArray``)
are not supported.

There is a need to clear the Epics buffer before using the ``caget`` command for the string, spectrum and image attributes.
Here is an example how to do this using pyepics:

.. code-block:: console

    def clear_epics_global_pv_cache():
        from epics.pv import _PVcache_
        for pv in list(_PVcache_.values()):
            pv.disconnect()
        _PVcache_.clear()
