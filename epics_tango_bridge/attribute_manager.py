# ------------------------------------------------------------------------------
#   epics-tango-bridge
#   Copyright (C) 2022  S2Innovation <contact@s2innovation.com>

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

import threading

import PyTango as tango

from epics_tango_bridge.logger import LoggerWithPvName
from epics_tango_bridge.task import make_task


def to_float_or_zero(number_as_string):
    try:
        return float(number_as_string)
    except Exception:
        return 0.0


def tango_to_epics_unit(unit):
    # before Tango @4aac0ccbc (8.x), "No unit" is returned by default
    if unit == tango.constants.UnitNotSpec:
        return ""
    else:
        return unit


class AttributeManager(object):  # pylint: disable=too-many-instance-attributes

    def __init__(self, driver, device_proxy, attribute, pvname, pvinfo): # noqa E501 pylint: disable=too-many-arguments
        self.__log = LoggerWithPvName(__name__, pvname)
        self._driver = driver
        self._device_proxy = device_proxy
        self._attribute = attribute
        self._pvinfo = pvinfo
        self._state_lock = threading.Lock()
        self.__write_task = make_task(
            self.write_thread,
            in_thread=pvinfo.get("asyn", False))
        self.__image_format = False
        self.__image_dim = 1
        self.__attr_conf_event = self._subscribe_to_attr_conf_event()

    def read(self):
        attribute = self._device_proxy.read_attribute(self._attribute)
        self._update_image_opts_from_attr_info(attribute)
        return attribute.value

    def write(self, value):
        self.__write_task(value)
        return True

    def _store_image_format(self, data_format):
        self.__image_format = data_format == tango.AttrDataFormat.IMAGE

    def _store_image_dim(self, dim):
        if dim > 0:
            self.__image_dim = dim

    def _update_image_opts_from_attr_conf(self, attr_conf):
        with self._state_lock:
            self._store_image_format(attr_conf.data_format)
            self._store_image_dim(attr_conf.max_dim_x)

    def _update_image_opts_from_attr_info(self, attr_info):
        with self._state_lock:
            self._store_image_format(attr_info.data_format)
            self._store_image_dim(attr_info.dim_x)

    def _reshape_image_data(self, value):
        with self._state_lock:
            image_format = self.__image_format
            image_dim = self.__image_dim
        if image_format:
            self.__log.info("Reshaping value, x dim %d", image_dim)
            tail = len(value) % image_dim
            value += [0] * (image_dim - tail if tail > 0 else 0)
            rows = [iter(value)] * image_dim
            cols = zip(*rows)
            value = list(map(list, cols))
            self.__log.info("Reshaped, value %s", value)
            return value
        else:
            return value

    def write_thread(self, value):
        try:
            self.__log.info("Writing value %s", value)
            value = self._reshape_image_data(value)
            self._device_proxy.write_attribute(self._attribute, value)

            readback = self.read()
            self.__log.info("Write completed, readback %s", readback)
        except Exception:
            self.__log.exception("Write failure, value %s", value)
        else:
            self._driver.write_pv_and_notify(readback)
        finally:
            self._driver.notify_write_pv_finished()

    def _subscribe_to_attr_conf_event(self):
        try:
            return self._device_proxy.subscribe_event(
                self._attribute,
                tango.EventType.ATTR_CONF_EVENT,
                self.handle_attr_conf_event)
        except Exception:
            self.__log.exception(
                "Failure during subscription to attr conf event")

    def handle_attr_conf_event(self, event):
        if event.err:
            self.__log.error("Attr conf event has error flag, %s", event)
        else:
            self._update_pvinfo(event.attr_conf)
            self._update_image_opts_from_attr_conf(event.attr_conf)

    def _update_pvinfo(self, attr_conf):
        pvinfo = self._make_new_pvinfo(attr_conf)
        self._driver.write_pv_info_and_notify(pvinfo)

    def _make_new_pvinfo(self, attr_conf):
        info = attr_conf.alarms
        pvinfo = {
            'unit': tango_to_epics_unit(attr_conf.unit),
            'low': to_float_or_zero(info.min_warning),
            'high': to_float_or_zero(info.max_warning),
            'lolo': to_float_or_zero(info.min_alarm),
            'hihi': to_float_or_zero(info.max_alarm),
        }
        return {k: v for k, v in pvinfo.items() if k not in self._pvinfo}


class PolledAttributeManager(AttributeManager):

    def __init__(self, driver, device_proxy, attribute, pvname, pvinfo):  # noqa E501 pylint: disable=too-many-arguments
        super(PolledAttributeManager, self).__init__(
            driver,
            device_proxy,
            attribute,
            pvname,
            pvinfo)
        self.__log = LoggerWithPvName(__name__, pvname)
        self.__change_event = self._subscribe_to_change_event()

    def read(self):
        return self._driver.read_pv()

    def _subscribe_to_change_event(self):
        try:
            return self._device_proxy.subscribe_event(
                self._attribute,
                tango.EventType.CHANGE_EVENT,
                self.handle_change_event)
        except Exception:
            self.__log.exception("Failure during subscription to change event")

    def handle_change_event(self, event):
        if event.err:
            self.__log.error("Change event has error flag, %s", event)
        else:
            self._driver.write_pv_and_notify(event.attr_value.value)
