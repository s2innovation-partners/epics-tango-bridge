# ------------------------------------------------------------------------------
#   epics-tango-bridge
#   Copyright (C) 2022  S2Innovation <contact@s2innovation.com>

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

import threading

from epics_tango_bridge.logger import LoggerWithPvName


def make_driver_adapter(driver, pvname, is_dummy=False):
    if is_dummy:
        return DummyDriverAdapter()
    else:
        return DriverAdapter(driver, pvname)


class DummyDriverAdapter(object):  # pylint: disable=too-few-public-methods

    def write_pv_and_notify(self, _value):
        pass


class DriverAdapter(object):

    def __init__(self, driver, pvname):
        self.__log = LoggerWithPvName(__name__, pvname)
        self.__driver = driver
        self.__pvname = pvname
        self.__lock = threading.Lock()

    def read_pv(self):
        with self.__lock:
            return self.__driver.getParam(self.__pvname)

    def write_pv_and_notify(self, value):
        try:
            with self.__lock:
                self.__driver.setParam(self.__pvname, value)
                self.__driver.updatePV(self.__pvname)
        except Exception:
            self.__log.exception("Failed to update value: %s", value)

    def write_pv_info_and_notify(self, info):
        try:
            with self.__lock:
                self.__driver.setParamInfo(self.__pvname, info)
                self.__driver.updatePV(self.__pvname)
        except Exception:
            self.__log.exception("Failed to update info: %s", info)

    def notify_write_pv_finished(self):
        try:
            with self.__lock:
                self.__driver.callbackPV(self.__pvname)
        except Exception:
            self.__log.exception("Failed to notify write completion")
