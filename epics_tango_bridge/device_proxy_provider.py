# ------------------------------------------------------------------------------
#   epics-tango-bridge
#   Copyright (C) 2022  S2Innovation <contact@s2innovation.com>

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

import PyTango as tango


def DeviceProxyProvider():  # pylint: disable=invalid-name

    cache = {}

    def get(device_name):
        if device_name in cache:
            return cache[device_name]
        else:
            proxy = tango.DeviceProxy(device_name)
            cache[device_name] = proxy
            return proxy

    return get
