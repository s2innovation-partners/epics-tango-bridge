# ------------------------------------------------------------------------------
#   epics-tango-bridge
#   Copyright (C) 2022  S2Innovation <contact@s2innovation.com>

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

import logging
import argparse
import runpy

import pcaspy

from epics_tango_bridge.tango_driver import TangoDriver
from epics_tango_bridge.signal_handler import SignalHandler
from epics_tango_bridge import __version__, __description__, __warranty__, __conditions__


logger = logging.getLogger(__name__)


def _parse_arguments():
    parser = argparse.ArgumentParser(
        description=__description__)

    parser.add_argument(
        dest="pvdb",
        metavar="PVDB",
        help="load PVs from %(metavar)s file")

    parser.add_argument(
        "-V",
        "--version",
        action="version",
        version="%(prog)s {}".format(__version__))

    parser.add_argument(
        "-W",
        "--warranty",
        action="version",
        version="%(prog)s {}".format(__warranty__),
        help="show details of Disclaimer of Warranty")

    parser.add_argument(
        "-C",
        "--conditions",
        action="version",
        version="%(prog)s {}".format(__conditions__),
        help="shows details under which conditions can be redistributed")

    parser.add_argument(
        "--log-level",
        dest="log_level",
        default='INFO',
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        metavar="LVL",
        help="set logging level to %(metavar)s (default %(default)s)")

    parser.add_argument(
        "--prefix",
        dest="prefix",
        default="tango",
        metavar="PREFIX",
        help="use %(metavar)s to prefix PV names (default %(default)s)")

    return parser.parse_args()


def _initialize_logging(level):
    logging.basicConfig(
        format='%(asctime)s %(levelname)s [%(threadName)s] %(filename)s:%(lineno)d: %(message)s',  # noqa E501 pylint: disable=line-too-long
        level=level)


def _load_pvdb(path):
    pvdb_module = runpy.run_path(path)
    return pvdb_module["pvdb"]


def _run_server(prefix, pvdb):
    try:
        signal = SignalHandler()
        server = pcaspy.SimpleServer()
        server.createPV(prefix, pvdb)
        driver = TangoDriver(pvdb)  # noqa F841,E501 pylint: disable=unused-variable
    except Exception:
        logger.exception("Failed to initialize the server")
    else:
        logger.info("Server is running")
        while not signal.stop:
            server.process(0.1)
        logger.info("Shutting down the server")


def run():
    options = _parse_arguments()
    log_level = getattr(logging, options.log_level)
    _initialize_logging(log_level)
    try:
        pvdb = _load_pvdb(options.pvdb)
    except Exception:
        logger.exception("Failed to load PVDB from {}".format(options.pvdb))
    else:
        prefix = options.prefix + ":"
        _run_server(prefix, pvdb)
