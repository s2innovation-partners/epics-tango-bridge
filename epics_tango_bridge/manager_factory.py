# ------------------------------------------------------------------------------
#   epics-tango-bridge
#   Copyright (C) 2022  S2Innovation <contact@s2innovation.com>

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

from collections import namedtuple

from epics_tango_bridge.device_proxy_provider import DeviceProxyProvider
from epics_tango_bridge.attribute_manager import \
    AttributeManager, \
    PolledAttributeManager
from epics_tango_bridge.command_manager import \
    CommandOutManager, \
    CommandInManager
from epics_tango_bridge.driver_adapter import make_driver_adapter


PV_SEGMENT_SEPARATOR = ":"

PV_SCOPE_ATT = "attr"
PV_SCOPE_CMD = "cmd"

PV_CMD_SLOT_IN = "in"
PV_CMD_SLOT_OUT = "out"


def make_attribute_manager(opts):
    if opts.pvinfo.get("-tg-polled", False):
        return PolledAttributeManager(
            make_driver_adapter(opts.driver, opts.pvname),
            opts.proxy,
            opts.target,
            opts.pvname,
            opts.pvinfo)
    else:
        return AttributeManager(
            make_driver_adapter(opts.driver, opts.pvname),
            opts.proxy,
            opts.target,
            opts.pvname,
            opts.pvinfo)


def make_command_in_manager(opts):
    pvname_out = PV_SEGMENT_SEPARATOR.join([
        opts.device, PV_SCOPE_CMD, opts.target, PV_CMD_SLOT_OUT])
    has_void_out = bool(opts.pvinfo.get("-tg-void-out", False))
    return CommandInManager(
        make_driver_adapter(opts.driver, opts.pvname),
        make_driver_adapter(opts.driver, pvname_out, is_dummy=has_void_out),
        opts.proxy,
        opts.target,
        opts.pvname,
        opts.pvinfo)


def make_command_out_manager(opts):
    return CommandOutManager(
        make_driver_adapter(opts.driver, opts.pvname),
        opts.pvname)


SCOPE_MANAGERS = {
    (PV_SCOPE_ATT, ()): make_attribute_manager,
    (PV_SCOPE_CMD, (PV_CMD_SLOT_IN,)): make_command_in_manager,
    (PV_SCOPE_CMD, (PV_CMD_SLOT_OUT,)): make_command_out_manager,
    }


def ManagerFactory(driver):  # pylint: disable=invalid-name

    device_proxy_provider = DeviceProxyProvider()

    def create(pvname, pvinfo):
        segments = pvname.split(PV_SEGMENT_SEPARATOR)
        [device, scope, target] = segments[:3]
        rest = tuple(segments[3:])
        device_proxy = device_proxy_provider(device)
        opts = {
            'device': device,
            'target': target,
            'driver': driver,
            'pvname': pvname,
            'pvinfo': pvinfo,
            'proxy': device_proxy,
        }
        return SCOPE_MANAGERS[(scope, rest)](
            namedtuple("ManagerOpts", opts.keys())(*opts.values()))

    return create
