# ------------------------------------------------------------------------------
#   epics-tango-bridge
#   Copyright (C) 2022  S2Innovation <contact@s2innovation.com>

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

from epics_tango_bridge.logger import LoggerWithPvName
from epics_tango_bridge.task import make_task


class CommandOutManager(object):

    def __init__(self, driver, pvname):
        self.__log = LoggerWithPvName(__name__, pvname)
        self.__driver = driver

    def read(self):
        return self.__driver.read_pv()

    def write(self, _value):
        self.__log.warning("Attempt to write to output slot")
        return False


class CommandInManager(object):  # pylint: disable=too-many-instance-attributes

    def __init__(  # pylint: disable=too-many-arguments
            self,
            driver,
            driver_out,
            device_proxy,
            command,
            pvname,
            pvinfo):
        self.__log = LoggerWithPvName(__name__, pvname)
        self.__driver = driver
        self.__driver_out = driver_out
        self.__device_proxy = device_proxy
        self.__command = command
        self.__void_in = bool(pvinfo.get("-tg-void-in", False))
        self.__execution_task = make_task(
            self.run_thread,
            in_thread=pvinfo.get("asyn", False))

    def read(self):
        return self.__driver.read_pv()

    def write(self, value):
        self.__execution_task(value)
        return True

    def _run_tango_command(self, argin):
        if self.__void_in:
            self.__log.info("Running with void input")
            result = self.__device_proxy.command_inout(self.__command)
        else:
            self.__log.info("Running with input %s", argin)
            result = self.__device_proxy.command_inout(self.__command, argin)
        self.__log.info("Execution completed, result %s", result)
        return result

    def run_thread(self, value):
        try:
            result = self._run_tango_command(value)
        except Exception:
            self.__log.exception("Execution failure, input %s", value)
        else:
            self.__driver.write_pv_and_notify(value)
            self.__driver_out.write_pv_and_notify(result)
        finally:
            self.__driver.notify_write_pv_finished()
