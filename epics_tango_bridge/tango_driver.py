# ------------------------------------------------------------------------------
#   epics-tango-bridge
#   Copyright (C) 2022  S2Innovation <contact@s2innovation.com>

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

import logging

import pcaspy

from epics_tango_bridge.manager_factory import ManagerFactory


logger = logging.getLogger(__name__)


def _make_managers(driver, pvdb):
    manager_factory = ManagerFactory(driver)
    for pvname, pvinfo in pvdb.items():
        try:
            yield pvname, manager_factory(pvname, pvinfo)
        except Exception:
            logger.exception("Failed to create manager for {}".format(pvname))


class TangoDriver(pcaspy.Driver):

    def __init__(self, pvdb):
        super(TangoDriver, self).__init__()
        self.__managers = {k: v for k, v in _make_managers(self, pvdb)}
        logger.info(
            "Driver initialization completed, number of PVs {}".format(
                len(self.__managers)))

    def read(self, reason):
        logger.debug("Dispatching read {}".format(reason))
        try:
            return self.__managers[reason].read()
        except Exception:
            logger.exception("Failure during read {}".format(reason))
            return self.getParam(reason)

    def write(self, reason, value):
        logger.debug("Dispatching write {}".format(reason))
        try:
            return self.__managers[reason].write(value)
        except Exception:
            logger.exception(
                "Failure during write {}, value {}".format(reason, value))
            return False
