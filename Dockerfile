FROM pklaus/epics_base:7.0.4_debian

USER root
RUN apt-get update && \
      apt-get -y install sudo

RUN echo "test"

RUN apt install -y pkg-config
RUN apt-get install -y libboost-python-dev


RUN apt-get install -y debconf
RUN echo "tango-common tango-common/tango-host string ${tango-databaseds}:{10000}"| debconf-set-selections
RUN cd /usr/local/bin && \
    curl -sO https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh && \
    chmod +x wait-for-it.sh
RUN apt-get install -y \
    pkg-config \
    python \
    python-setuptools \
    python-pytango \
    python-pip \
    python-lxml \
    python-future \
    tango-starter \
    git


RUN echo "test2"

COPY . /epics_tango_bridge
WORKDIR /epics_tango_bridge

RUN python setup.py install